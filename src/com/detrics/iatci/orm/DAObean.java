package com.detrics.iatci.orm;

/*
 *
 * 
*/

import java.beans.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import com.sun.rowset.CachedRowSetImpl;

/**
 *
 * @author  martijn
 */
public abstract class DAObean implements java.io.Serializable {

    protected boolean valid = false;
    protected static transient DataSource ds;
    
    public DAObean() {
    	if (ds==null) {
	        try {
	            Context ctx = new InitialContext();
	            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/iatci");
	            ctx.close();
	        } catch(Exception e) { 
	        	e.printStackTrace();
	        }
    	}
    }

    public abstract void findByPK(int PK) throws Exception;

    public boolean isValid() {
        return valid;
    }

    public void dropValid() {
    	this.valid = false;
    }
    
    protected RowSet getQuery(String s) throws Exception {
        Connection con = null;
        java.sql.Statement st = null;
        ResultSet rs = null;
        CachedRowSetImpl cr = new CachedRowSetImpl();
        try {
            con = ds.getConnection();
            st = con.createStatement();
            rs = st.executeQuery(s);
            cr.populate(rs);
            rs.close();
            st.close();
            return cr;
        } catch(Exception e1) {
        	throw e1;
        }
        finally {
            if (st!=null) st.close();
            if (con!=null) con.close();
            con = null;
            st = null;
        }
    }

    protected void insert(String insert) throws Exception {
        Connection con = null;
        java.sql.Statement s = null;
        try {
            con = ds.getConnection();
            s = con.createStatement();
            s.execute(insert);
        } catch(Exception e) {
            throw e;
        } finally {
            try {
                if (s!=null) s.close();
                if (con!=null) con.close();
                con = null;
                s = null;
            }
            catch (Exception e2) { throw e2; }
        }
    }

}
