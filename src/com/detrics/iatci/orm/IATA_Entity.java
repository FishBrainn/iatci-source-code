package com.detrics.iatci.orm;

public class IATA_Entity extends DAObean {

	private String airliner2 = "LH";
	private String airliner3;
	private String station3 = "FRA";
	private String country2 = "DE";
	
	public IATA_Entity() {
		
	}
	
	public IATA_Entity(String liner2, String liner3, String station3, String country) {
		this.airliner2= liner2;
		this.airliner3 = liner3;
		this.station3 = station3;
		this.country2 = country;
	}
	
	public String getAirliner2() {
		return airliner2;
	}
	public void setAirliner2(String airliner2) {
		this.airliner2 = airliner2;
	}
	public String getAirliner3() {
		return airliner3;
	}
	public void setAirliner3(String airliner3) {
		this.airliner3 = airliner3;
	}
	public String getStation3() {
		return station3;
	}
	public void setStation3(String station3) {
		this.station3 = station3;
	}
	public String getCountry2() {
		return country2;
	}
	public void setCountry2(String country2) {
		this.country2 = country2;
	}
	@Override
	public void findByPK(int PK) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
