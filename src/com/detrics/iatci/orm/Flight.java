package com.detrics.iatci.orm;

public class Flight extends DAObean {

	private String airliner;
	private String flightno;
	private IATA_Entity dept;
	private IATA_Entity arr;
	private String outblocaldatetime;	//  yyMMddhhmm
	private String inblocaldatetime;	//  yyMMddhhmm
	
	public Flight createFlight(String carrier, String flight, IATA_Entity dept, IATA_Entity arr, String eta, String etd) {
		Flight f = new Flight();
		f.setAirliner(carrier);
		f.setFlightno(flight);
		f.setArr(arr);
		f.setDept(dept);
		f.setETA(eta);
		f.setETD(etd);
		return f;
	}
	
	public String getAirliner() {
		return airliner;
	}
	public void setAirliner(String airliner) {
		this.airliner = airliner;
	}
	public String getFlightno() {
		return flightno;
	}
	public void setFlightno(String flightno) {
		this.flightno = flightno;
	}
	public IATA_Entity getDept() {
		return dept;
	}
	public void setDept(IATA_Entity dept) {
		this.dept = dept;
	}
	public IATA_Entity getArr() {
		return arr;
	}
	public void setArr(IATA_Entity arr) {
		this.arr = arr;
	}
	public String getETD() {
		return outblocaldatetime;
	}
	public void setETD(String outblocaldatetime) {
		this.outblocaldatetime = outblocaldatetime;
	}
	public String getETA() {
		return inblocaldatetime;
	}
	public void setETA(String inblocaldatetime) {
		this.inblocaldatetime = inblocaldatetime;
	}
	@Override
	public void findByPK(int PK) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
