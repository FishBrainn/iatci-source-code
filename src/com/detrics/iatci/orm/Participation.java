package com.detrics.iatci.orm;

import javax.sql.RowSet;

public class Participation extends DAObean {

	private int partID;
	private String paxname;
	private String paxfirstname;
	private String gender;
	private String TKNE;
	private String paxIDrespondingSystem="";
	private String paxIDqueryingSystem="";
	private String paxID3rdSystem="";
	private boolean hasInfant;
	private String seatclass;
	private String recloc;
	
	@Override
	public void findByPK(int PK) throws Exception {
		RowSet rs = this.getQuery("SELECT * FROM iatci_pax WHERE paxID="+PK);
		if (rs.next()) {
			this.partID = PK;
			this.paxname = rs.getString("name");
			this.paxfirstname = rs.getString("fname");
			this.gender = rs.getString("gender");
			this.TKNE = rs.getString("TKNE");
			this.hasInfant = rs.getInt("hasinfant")>0;
			this.seatclass = rs.getString("seatclass");
			this.recloc = rs.getString("recloc");
		}
	}

	public Participation createPax(String name, String fname, String gender, String TKNE, String seatclass, String rl) {
		Participation p = new Participation();
		p.setPaxname(name);
		p.setPaxfirstname(fname);
		p.setGender(gender);
		p.setTKNE(TKNE);
		p.setSeatclass(seatclass);
		p.setRecloc(rl);
		return p;
	}
	
	public void persistPax(int sessionID) throws Exception {
		this.insert("INSERT INTO iatci_pax (p_sessionID, name, fname, gender, TKNE, seatclass, recloc) VALUES("+sessionID+", '"+paxname+"', '"+paxfirstname+"', '"+gender+"', '"+TKNE+"', '"+seatclass+"', '"+recloc+"')");
	}
	
	public int getPartID() {
		return partID;
	}

	public void setPartID(int partID) {
		this.partID = partID;
	}

	public String getPaxname() {
		return paxname;
	}

	public void setPaxname(String paxname) {
		this.paxname = paxname;
	}

	public String getPaxfirstname() {
		return paxfirstname;
	}

	public void setPaxfirstname(String paxfirstname) {
		this.paxfirstname = paxfirstname;
	}

	public boolean hasInfant() {
		return this.hasInfant;
	}
	
	public void setHasInfant(boolean b) {
		this.hasInfant = b;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPaxIDrespondingSystem() {
		return paxIDrespondingSystem;
	}

	public void setPaxIDrespondingSystem(String paxIDrespondingSystem) {
		this.paxIDrespondingSystem = paxIDrespondingSystem;
	}

	public String getPaxIDqueryingSystem() {
		return paxIDqueryingSystem;
	}

	public void setPaxIDqueryingSystem(String paxIDqueryingSystem) {
		this.paxIDqueryingSystem = paxIDqueryingSystem;
	}

	public String getPaxID3rdSystem() {
		return paxID3rdSystem;
	}

	public void setPaxID3rdSystem(String paxID3rdSystem) {
		this.paxID3rdSystem = paxID3rdSystem;
	}

	public String getTKNE() {
		return TKNE;
	}

	public void setTKNE(String tKNE) {
		TKNE = tKNE;
	}

	public String getSeatclass() {
		return seatclass;
	}
	
	public void setSeatclass(String sc) {
		this.seatclass = sc;
	}
	
	public String getRecloc() {
		return recloc;
	}
	
	public void setRecloc(String sc) {
		this.recloc = sc;
	}
	
}
