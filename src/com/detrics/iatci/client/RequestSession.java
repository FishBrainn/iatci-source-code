package com.detrics.iatci.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Collection;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.detrics.iatci.*;
import com.detrics.iatci.IOS.IOS_Session;
import com.detrics.iatci.orm.*;

/**
 * Servlet implementation class RequestSession
 */
@WebServlet("/RequestSession")
public class RequestSession extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestSession() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("resID")!=null) {
			try {
				int resID = Integer.parseInt(request.getParameter("resID"));
				String sessionID = new IOS_Session().createSession(resID);
				response.setContentType("text/html");
			    PrintWriter out = response.getWriter();
			    out.write("sessionID="+sessionID);
			    out.close();
				return;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		/* Decrepated, session is now done on actual IOS-reservation
		try {
			Session session = new Session();
			Participation p1 = new Participation();
			p1.setPaxname("BENNET");
			p1.setPaxfirstname("CLEMIELOU");
			p1.setPaxIDqueryingSystem("3009050010");
			p1.setPaxIDrespondingSystem("");
			p1.setPaxID3rdSystem("");
			p1.setGender("M");
			
			Participation p2 = new Participation();
			p2.setPaxname("BENNET");
			p2.setPaxfirstname("BLAINB");
			p2.setPaxIDqueryingSystem("3009050011");
			p2.setPaxIDrespondingSystem("");
			p2.setPaxID3rdSystem("");
			p2.setGender("M");

			Flight f1 = new Flight();
			Flight f2 = new Flight();
			
			f1.setAirliner("SK");
			f1.setFlightno("111");
			f1.setInblocaldatetime("110601");
			f1.setOutblocaldatetime("1106010930");
			f1.setDept(new IATA_Entity("SK", "", "CPH", "DK"));
			f1.setArr(new IATA_Entity("SK", "", "ORD", "IT"));

			f2.setAirliner("UA");
			f2.setFlightno("222");
			f2.setOutblocaldatetime("110603");
			f2.setDept(new IATA_Entity("SK", "", "ORD", "IT"));
			f2.setArr(new IATA_Entity("UA", "", "YYZ", "AB"));
			
			Collection<Participation> pax = new Vector<Participation>();
			pax.add(p1);
			pax.add(p2);
			String sessionID = session.createSession(Message.DCQCKI,  pax, new IATA_Entity("SK","","",""), new IATA_Entity("","HUB","",""), f1, f2);

			response.setContentType("text/html");
		    PrintWriter out = response.getWriter();
		    out.write("sessionID="+sessionID);
		    out.close();
			return;
		} catch(Exception e) {
			e.printStackTrace();
		}*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
