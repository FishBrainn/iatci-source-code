package com.detrics.iatci.broker;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import com.detrics.iatci.Session;
import com.detrics.iatci.edi.DCRCKA;

/**
 * This class is a MOCK-object, acting as a downward host, responding to msgs received via the Hub (the other MOCK-object).
 * It will return a msg to the iatci-Broker hook. Actually the mock HUB must do that ..
 * 
 * @author martijn
 *
 */
public class MockBroker extends Thread {

	public String request;
	public String response;
	
	public MockBroker(String request) {
		//DCRCKA hubresponse = new DCRCKA(msg);
		this.request = request;
		Session se = new Session();
		String sID = "";
		try {
			se.findByMsg(request);
			sID = se.getSessionID();
		} catch(Exception e) {
			e.printStackTrace();
		}
		StringBuffer s = new StringBuffer();
		s.append("UNB+IATA:1:+UA+SK+110301:0158+"+sID+"'");
		s.append("UNH+RTZDCS+DCRCKA:03:1:IA+"+sID+"'");
		s.append("FDR+UA+222+1106011135+ORD+YYZ+1106011525+T'");
		s.append("WAD+1:91'");
		this.response = s.toString();
		System.out.println("HUB created response");
	}
	
	private boolean sendReponseBack() throws Exception {
		System.out.println("Going to send response");
        URL url = new URL("http://localhost:8080/iatciBroker/ios/HubListener?response="+URLEncoder.encode(response, "ISO-8859-1"));
        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
        String inputLine;
        System.out.println("Receiving HUBclient says : ");
        while ((inputLine = in.readLine()) != null) 
            System.out.println(inputLine);
        in.close();
        return true;
	}

	public void run() {
		try {
			Thread.sleep(10000);
		    this.sendReponseBack();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	
}
