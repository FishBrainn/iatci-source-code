package com.detrics.iatci.broker;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.detrics.iatci.Message;
import com.detrics.iatci.edi.DCRCKA;

/**
 * 
 * This is a MOCK-class, a helper-class for testing IATCI-functionality.
 * It is the testing-endpoint for a Session to send a message to.
 * It just expects a message in plain ISO-8859-1 EDI format, as a get-parameter called 'msg'.
 * 
 * It will have several msgtype-hooks.
 * 
 * Every received message will be forwarded to the MockBroker, acting as a downward station, responding to this message.
 * This MockBroker will look at the message, and give a default response, later enhanced with test-flags for returning an error,
 * warning, or refusal etc.
 * 
 * Servlet implementation class Hub
 */
@WebServlet("/iatci/Hub")
public class Hub extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Hub() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msgtype = request.getParameter("msgtype");
		System.out.println("HUB says : RCVD MSG");
		String msg = request.getParameter("msg");
		msg = URLDecoder.decode(msg, "ISO-8859-1");
		msg = msg.replace(" " ,  "+");
		if (msgtype.equals(Message.DCQCKI)) {
			System.out.println("HUB says : RCVD DCQCKI");
			System.out.println("DCQCKI = "+msg);
			MockBroker m = new MockBroker(msg);
			response.setContentType("text/html");
		    PrintWriter out = response.getWriter();
		    out.write("REQUEST OK");
		    out.close();
			m.start();
		    return;
		}
		if (msgtype.equals(Message.DCRCKA)) {
			
		}
		response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    out.write("NOK");
	    out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
