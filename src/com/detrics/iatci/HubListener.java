package com.detrics.iatci;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.detrics.iatci.edi.DCQCKI;
import com.detrics.iatci.edi.DCRCKA;

/**
 * Servlet implementation class HubListener
 * 
 * MOCK HUB
 * 
 */
@WebServlet("/ios/HubListener")
public class HubListener extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HubListener() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msgstring = request.getParameter("response");
		String type = "";
		Session s = new Session();
		try {
			System.out.println("FROM HUB: "+msgstring);
			response.setContentType("text/html");
		    PrintWriter out = response.getWriter();
		    out.write("RESPONSE OK");
		    out.close();
	
		    // handle incoming msg
			s.findByMsg(msgstring);
			Message m;
			if (msgstring.contains("DCRCKA")) {
				type = Message.DCRCKA;
				int msgID = s.addMsg("I",  type,  msgstring);
				s.setPendingOut(false);
				m = new DCRCKA(msgID, msgstring);
				m.parse();
			}
			if (msgstring.contains("DCQCKI")) {
				type = Message.DCQCKI;
				int msgID = s.addMsg("I",  type,  msgstring);
				s.setPendingIn(true);
				m = new DCQCKI(msgID, msgstring);
				m.parse();
			}
		} catch(Exception e) {
			//TODO escalate properly
			e.printStackTrace();
		}
	    
	    return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
