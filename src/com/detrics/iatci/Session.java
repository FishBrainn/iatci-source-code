package com.detrics.iatci;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.Vector;

import javax.sql.RowSet;

import com.detrics.iatci.broker.Hub;
import com.detrics.iatci.edi.*;
import com.detrics.iatci.orm.*;
import com.detrics.iatci.utils.UTCnexus;

/*
 * IATCI session wrapper-class
 * 
 * This is the holding class for an iatci-session.
 * It contains a request and a response
 * 
 * db-structure:
 * isID
 * sessionID
 * sessionfromID
 * sessiontoID
 * iflightID 
 */
public class Session extends DAObean {

	private int isessionID;
	private String sessionID;
	private IATA_Entity isender = new IATA_Entity();
	private IATA_Entity irecipient = new IATA_Entity();
	
	private Flight originFlight;				// flight departing from current station
	private Flight connectingFlight;		// connecting flight for which info is requested (like checkin etc)

	private Message request;
	private Collection<Message> msgs;
	private Collection<Participation> pax;
	private int iosresID;			// reservationID in IOS-meredon
	private long created;			// timestamp of creation in UTC
	private int haswarnings;		// flag indicating warnings in this session
	private int haserrors;			// flag indicating errors in this session
	private int pending_in;			// flag indicating that session-status is waiting for DCS
	private int pending_out;		// flag indicating that session-status is waiting for HUB
	
	/**
	 * Finds the session by the given hex sessionID
	 * @param hsessionID
	 * @throws Exception
	 */
	public void findByPK(String hsessionID) throws Exception {
		int PK = Integer.parseInt(hsessionID, 16);
		this.findByPK(PK);
	}

	@Override
	public void findByPK(int PK) throws Exception {
		RowSet rs = this.getQuery("SELECT * FROM iatcisession WHERE isID="+PK);
		if (rs.next()) {
			this.isessionID = PK;
			this.sessionID = rs.getString("sessionID");
			this.created = rs.getLong("sts");
			this.pending_in = rs.getInt("pendingin");
			this.pending_out = rs.getInt("pendingout");
			this.iosresID = rs.getInt("iosresID");
			Collection<Message> list = this.getMessages();
			if (list.size()>0) {
				request = (Message)list.iterator().next();
			}
		}
	}

	/**
	 * Creates a new IATCI-Session and returns the ID of it.
	 * This ID is used by the client to request status-updates etc.
	 * 
	 * @return the ID of the created Session (in hex)
	 * @throws Exception
	 */
	public String createSession(String msgtype, Collection<Participation> givenpax, IATA_Entity isender, IATA_Entity irecipient, Flight origin, Flight conn) throws Exception {
		this.isessionID = this.getNextSessionID();
		this.sessionID = Integer.toHexString(isessionID);	// hex
		sessionID = sessionID.toUpperCase();
		this.created = UTCnexus.getCurrentUTCtime();
		this.insert("INSERT INTO iatcisession (isID, sessionID, sessionfromID, sessiontoID, iflightID, sts) VALUES("+isessionID+", '"+sessionID+"', 1, 2, 0, "+created+")");
		this.pax = givenpax;
		for (Participation p : pax) { p.persistPax(isessionID); }
		this.isender = isender;
		this.irecipient = irecipient;
		this.originFlight = origin;
		this.connectingFlight = conn;
		if (msgtype.equals(Message.DCQCKI)) {
			request = new DCQCKI();
			request.generateMessage(this);
		}
		this.addMsg("O", Message.DCQCKI, request.getMessage());
		return this.sessionID;
	}

	public void start() throws Exception {
		Hub hub = new Hub();
		this.sendMessageToHub(request);
	}
	
	public String getSessionID() {
		return this.sessionID;
	}
	
	public IATA_Entity getISender() {
		return this.isender;
	}
	
	public IATA_Entity getiRecipient() {
		return this.irecipient;
	}
	
	public Flight getConnectionFlight() {
		return this.connectingFlight;
	}

	public Flight getOriginFlight() {
		return this.originFlight;
	}
	
	private boolean sendMessageToHub(Message msg) {
		try {
			boolean succes = sendMessageToTestHubIP(msg.getMessageType(), msg.getMessage());
			this.setPendingOut(true);
			return true;
		} catch(Exception e) {
			//TODO escalate
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * For testing-purposes on local MOCK HUB object
	 * 
	 * @param msgtype
	 * @param msg
	 * @return
	 * @throws MalformedURLException
	 * @see com.detrics.iatci.broker.Hub
	 */
	private boolean sendMessageToTestHubIP(String msgtype, String msg) throws MalformedURLException {
        DataOutputStream output = null;
        DataInputStream input = null;
        try {
        	// if (test) { //
        	URL url = new URL("http://localhost:8080/iatciBroker/iatci/Hub?msgtype="+msgtype+"&msg="+URLEncoder.encode(msg, "ISO-8859-1"));
        	System.out.println(url.toString());
            URLConnection yc = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            String inputLine;
            System.out.println("HUB says : ");
            while ((inputLine = in.readLine()) != null) 
                System.out.println(inputLine);
            in.close();        	
        	
        } catch(Exception e) {
        	e.printStackTrace();
        } finally {
        	try {
	        	if (output!=null) output.close();
	        	if (input!=null) input.close();
        	} catch(Exception e) {
        		e.printStackTrace();
        	}
        }
        return true;
	}
	
	/**
	 * Returns next sessionID (in hex)
	 * @return
	 * @throws Exception
	 */
	private int getNextSessionID() throws Exception {
		RowSet rs = this.getQuery("SELECT sessionID FROM iatciincrs WHERE iiID=1");
		if (rs.next()) {
			int sessionID = rs.getInt("sessionID")+1;
			this.insert("UPDATE iatciincrs SET sessionID=sessionID+1 WHERE iiID=1");
			return sessionID;
		}
		return 0;
	}

	public int addMsg(String IO, String giventype, String givenmsg) throws Exception {
		RowSet rs = this.getQuery("SELECT MAX(msgidx) AS idx FROM iatci_msg WHERE p_session="+this.isessionID);
		int idx = 0;
		if (rs.next()) {
			idx = rs.getInt("idx");
		}
		idx++;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = ds.getConnection();
            ps = con.prepareStatement("INSERT INTO iatci_msg (io, p_session, msgidx, msgtype, msg, mts) VALUES(?,?,?,?,?,?)");
            ps.setString(1, IO);
            ps.setInt(2, this.isessionID);
            ps.setInt(3, idx);
            ps.setString(4, giventype);
            ps.setString(5, givenmsg);
            ps.setLong(6, UTCnexus.getCurrentUTCtime());
            ps.execute();
            RowSet rs2 = this.getQuery("SELECT msgID FROM iatci_msg WHERE p_session="+this.isessionID+" AND msgidx="+idx);
            if (rs2.next()) {
            	return rs2.getInt("msgID");
            }
        } catch(Exception e) {
            throw e;
        } finally {
            if (ps!=null) ps.close();
            if (con!=null) con.close();
        }
        return 0;
	}
	
	public Collection<Message> getMessages() throws Exception {
		Collection<Message> list = new Vector<Message>();
		RowSet rs = this.getQuery("SELECT * FROM iatci_msg WHERE p_session="+this.isessionID+" ORDER BY msgidx");
		while (rs.next()) {
			//Message msg = this.getInstance(rs.getString("msgtype"), rs.getString("msg"), rs.getInt("msgID"));
			Message msg = D.findByPK(rs.getInt("msgID"));
			list.add(msg);
		}
		return list;
	}
	
	public long getTS() {
		return this.created;
	}

	public String getUTCtime() {
		return UTCnexus.getIATAtime(this.created);
	}
	public String getUTCdate() {
		return UTCnexus.getIATAdate(this.created);
	}
	public String getTime() {
		return UTCnexus.getDefaultTime(this.created);
	}
	public String getDate() {
		return UTCnexus.getDefaultDate(this.created);
	}
	
	public Collection<Participation> getPax() {
		return this.pax;
	}
	
	public void addPax(Participation p) {
		if (this.pax==null) {
			pax = new Vector<Participation>();
		}
		pax.add(p);
	}

	public Message getInstance(String type, String msg, int msgID) throws Exception {
		if (type.equals(Message.DCQCKI)) return new DCQCKI(msgID, msg);
		if (type.equals(Message.DCRCKA)) return new DCRCKA(msgID, msg);
		return null;
	}

	/**
	 * Finds the session (of this broker) belonging to the given message
	 * @param s
	 * @throws Exception
	 */
	public void findByMsg(String s) throws Exception {
		String[] lines = s.split("'");
		String[] parts = lines[0].split("\\+");
		String hsessionID = parts[parts.length-1];
		this.findByPK(hsessionID);
	}
	
	public void setOriginFlight(Flight f) {
		this.originFlight= f;
	}
	public void setConntingFlight(Flight f) {
		this.connectingFlight = f;
	}

	public static Collection<Session> getSessions(int from) throws Exception {
		Vector<Session> list = new Vector<Session>();
		RowSet rs = new Session().getQuery("SELECT * FROM iatcisession ORDER BY sts DESC LIMIT "+from+", 20");
		while (rs.next()) {
			Session s = new Session();
			s.findByPK(rs.getInt("isID"));
			list.add(s);
		}
		return list;
	}
	
	public RowSet getQuery(String s) throws Exception {
		return super.getQuery(s);
	}
	public void insert(String s) throws Exception {
		super.insert(s);
	}
	
	public int getnWarnings() throws Exception {
		RowSet rs = this.getQuery("SELECT COUNT(*) AS n FROM iatci_msglog LEFT JOIN iatci_msg ON iatci_msglog.msgID=iatci_msg.msgID WHERE p_session="+this.isessionID);
		if (rs.next()) return rs.getInt("n");
		return 0;
	}
	
	public boolean isPendingIn() { return this.pending_in>0; }
	public boolean isPendingOut() { return this.pending_out>0; }

	public void setPendingOut(boolean pending) throws Exception {
		if (pending) {
			this.insert("UPDATE iatcisession SET pendingout=1 WHERE isID="+this.isessionID);
		} else {
			this.insert("UPDATE iatcisession SET pendingout=0 WHERE isID="+this.isessionID);
		}
	}
	public void setPendingIn(boolean pending) throws Exception {
		if (pending) {
			this.insert("UPDATE iatcisession SET pendingin=1 WHERE isID="+this.isessionID);
		} else {
			this.insert("UPDATE iatcisession SET pendingin=0 WHERE isID="+this.isessionID);
		}
	}
	
}
