package com.detrics.iatci;

import java.sql.Date;
import java.util.Collection;

import com.detrics.iatci.edi.EDIFormatException;
import com.detrics.iatci.edi.elements.WAD;
import com.detrics.iatci.utils.UTCnexus;

public interface Message {

	public final static String DCQCKI = "DCQCKI"; 
	public final static String DCRCKA = "DCRCKA";
	
	public String getMessageType();
	public void generateMessage(Session session) throws Exception;
	public String getMessage();
	public int getMsgID();
	public String getIO();
	public String getIOw();
	public boolean validateOutbound();
	public String getCreatedDate();
	public String getCreatedTime();
	public void addWarning(WAD wad);
	public void addError(WAD wad);
	public Collection<WAD> getWarnings() throws Exception;
	public void parse() throws EDIFormatException;
	
}
