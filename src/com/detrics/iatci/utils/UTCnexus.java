package com.detrics.iatci.utils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class UTCnexus {

	public static TimeZone utctimezone = TimeZone.getTimeZone("UTC");
	public static DateFormat iatatime = new SimpleDateFormat("HHmm", new Locale("UTC"));
	public static DateFormat iatadate = new SimpleDateFormat("yyMMdd", new Locale("UTC"));

	public static long getCurrentUTCtime() {
		Calendar cal = Calendar.getInstance(utctimezone);
		return cal.getTimeInMillis();
	}

	private static DateFormat defaultdate = new SimpleDateFormat("yyyy-MM-dd");
	private static DateFormat humandate = new SimpleDateFormat("dd MMM");
	private static DateFormat longdate = new SimpleDateFormat("EEE d MMM yyyy");
	private static DateFormat defaulttime = new SimpleDateFormat("HH:mm");
	private static DateFormat defaulthtime = new SimpleDateFormat("H'h'mm");
	private static DateFormat iatadatetime = new SimpleDateFormat("ddHHmm", new Locale("UTC"));
	private static TimeZone defaulttimezone = TimeZone.getDefault();
	//private static TimeZone defaulttimezone = new Incrementer().getMyTimeZone();
	public static final long DAYMILLIS = 1000 * 3600 * 24;

	/**
	 * Returns the given utc-time in format yyyy-MM-dd, in the default TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getDefaultDate(long UTCtime) {
		return defaultdate.format(UTCtime);
	}

	/**
	 * Returns the given utc-time in format yyyy-MM-dd, in the default TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getHumanDate(long UTCtime) {
		return humandate.format(UTCtime);
	}

	/**
	 * Returns the given utc-time in IATA SSIM format : HHmm
	 * @param UTCtime
	 * @return
	 */
	public static String getIATAtime(long UTCtime) {
		DateFormat utc2 = new SimpleDateFormat("HHmm");
		utc2.setTimeZone(utctimezone);
		return utc2.format(new Date(UTCtime));
	}

	/**
	 * Returns the given utc-date in IATA SSIM format : EEE dd MMM
	 * @param UTCtime
	 * @return
	 */
	public static String getIATAdate(long UTCtime) {
		DateFormat utc2 = new SimpleDateFormat("yyMMdd");
		utc2.setTimeZone(utctimezone);
		return utc2.format(new Date(UTCtime)).toUpperCase();
	}
	
	
	/**
	 * Returns the given utc-date in IATA SSIM format : EEE dd MMM
	 * @param UTCtime
	 * @return
	 */
	public static String getIATAdateE(long UTCtime) {
		DateFormat utc2 = new SimpleDateFormat("EEE dd MMM");
		utc2.setTimeZone(utctimezone);
		return utc2.format(new Date(UTCtime)).toUpperCase();
	}
	
	/**
	 * Returns the given utc-time in format EEE d MMM yyyy, in the default TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getLongDate(long UTCtime) {
		return longdate.format(UTCtime);
	}

	/**
	 * Returns the given utc-time in format HH:mm, in the default TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getDefaultTime(long UTCtime) {
		return defaulttime.format(UTCtime);
	}

	/**
	 * Returns the weekday (1=MON, 7=SUN) of the given utctime, based on locality by given boolean aslocal
	 * In Java v7, this can be done with pattern 'u' (day number of week, i.e. 1--7), however in Java v6 this pattern is not yet available.
	 * Since it's only used for checking DIFFERENCE with other weekday, julian day is ued here.
	 * @param UTCtime
	 * @param aslocal
	 * @return
	 */
	public static int getWeekday(long UTCtime, boolean aslocal) {
		//DateFormat zoneformat = new SimpleDateFormat("u");
		DateFormat zoneformat = new SimpleDateFormat("D");
		Date d = new Date(UTCtime);
		if (aslocal) {
			zoneformat.setTimeZone(utctimezone);
		} else {
			zoneformat.setTimeZone(defaulttimezone);
		}
		String su = zoneformat.format(d);
		return Integer.parseInt(su);
	}
	
	/**
	 * Returns the given utc-time in format yyyy-MM-dd, with locality in respect to the given TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getZoneDate(long UTCtime, TimeZone tz) {
		DateFormat zoneformat = new SimpleDateFormat("yyyy-MM-dd");
		zoneformat.setTimeZone(tz);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}

	/**
	 * Returns the given utc-time in format EEE d MMM yyyy, with locality in respect to the given TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getLongZoneDate(long UTCtime, TimeZone tz) {
		DateFormat zoneformat = new SimpleDateFormat("EEE d MMM yyyy");
		zoneformat.setTimeZone(tz);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}

	/**
	 * Returns the given utc-stamp in YYYY-MM-DD
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getMediumZoneDate(long UTCtime, TimeZone tz) {
		DateFormat zoneformat = new SimpleDateFormat("yyyy-MM-dd");
		zoneformat.setTimeZone(tz);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}

	/**
	 * Returns the given utc-time in format EEE d MMM yyyy, with locality in respect to the given TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getLongDefaultZoneDate(long UTCtime) {
		DateFormat zoneformat = new SimpleDateFormat("EEE d MMM yyyy");
		zoneformat.setTimeZone(defaulttimezone);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}
	
	public static String getLongDefaultZoneDateShort(long UTCtime) {
		DateFormat zoneformat = new SimpleDateFormat("EEE d MMM");
		zoneformat.setTimeZone(defaulttimezone);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}

	/**
	 * Returns the given utc-time in format HH:mm, with locality in respect to the given TimeZone
	 * @param UTCtime
	 * @param tz
	 * @return
	 */
	public static String getZoneTime(long UTCtime, TimeZone tz) {
		DateFormat zoneformat = new SimpleDateFormat("HH:mm");
		zoneformat.setTimeZone(tz);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}
	public static String getZoneTimeS(long UTCtime, TimeZone tz) {
		DateFormat zoneformat = new SimpleDateFormat("HHmm");
		zoneformat.setTimeZone(tz);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}

	/*
	 * Returns given UTC-time in dateformat ddd d MMM
	 */
	public static String getShortZoneDate(long UTCtime, TimeZone tz) {
		DateFormat zoneformat = new SimpleDateFormat("EEE d MMM");
		zoneformat.setTimeZone(tz);
		Date d = new Date(UTCtime);
		return zoneformat.format(d);
	}

	public static TimeZone getDefaultTimeZone() {
		return defaulttimezone;
	}

	public static String getDefaultTimeZoneShort() {
		return defaulttimezone.getDisplayName();
	}

	/**
	 * Returns UTC-timestamp, interpreting given date and time according to users TimeZone (default) and default formats.<br>
	 * Parsing of the time-parameter is loose: it may be given with or without seconds.<br>
	 * The returned value will be used by the calling code to write it to the utc-timestamps in the database.
	 * @param date
	 * @param time
	 * @return
	 */
	public static long getDefaultUTC(String date, String time) throws ParseException {
		StringBuffer toparse = new StringBuffer(date).append(" ").append(time);
		DateFormat utc = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat utc2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		utc.setTimeZone(defaulttimezone);
		utc2.setTimeZone(defaulttimezone);
		try {
			Date utcdate = utc.parse(toparse.toString());
			return utcdate.getTime();
		} catch(Exception e) {
			Date utcdate = utc2.parse(toparse.toString());
			return utcdate.getTime();
		}
	}

	/**
	 * Returns UTC-timestamp, interpreting given date and time according to given TimeZone (default) and default formats.<br>
	 * This value is written to the database.
	 * @param date
	 * @param time
	 * @return
	 */
	public static long getZoneUTC(String date, String time, TimeZone zone) throws ParseException {
		StringBuffer toparse = new StringBuffer(date).append(" ").append(time);
		DateFormat utc = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat utc2 = new SimpleDateFormat("yyyy-MM-dd HHmm");
		utc.setTimeZone(zone);
		utc2.setTimeZone(zone);
		try {
			Date utcdate = utc.parse(toparse.toString());
			return utcdate.getTime();
		} catch(Exception e) {
			Date utcdate = utc2.parse(toparse.toString());
			return utcdate.getTime();
		}
	}

	/**
	 * Returns UTC-timestamp, interpreting given date and time according to given TimeZone (default) and default formats.<br>
	 * This value is written to the database.
	 * @param date
	 * @param time
	 * @return
	 */
	public static long getUTC(String date, String time) throws ParseException {
		StringBuffer toparse = new StringBuffer(date).append(" ").append(time);
		DateFormat utc = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat utc2 = new SimpleDateFormat("yyyy-MM-dd HHmm");
		utc.setTimeZone(utctimezone);
		utc2.setTimeZone(utctimezone);
		try {
			Date utcdate = utc.parse(toparse.toString());
			return utcdate.getTime();
		} catch(Exception e) {
			Date utcdate = utc2.parse(toparse.toString());
			return utcdate.getTime();
		}
	}

	/**
	 * Returns the UTC-stamp of the LOCAL time of 0:00 
	 * @return
	 */
	public static long getCurrentUTCt0() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.HOUR_OF_DAY,  0);
		cal.set(cal.MINUTE, 0);
		return cal.getTimeInMillis();
	}
	public static long getCurrentUTCt8() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.HOUR_OF_DAY,  8);
		cal.set(cal.MINUTE, 0);
		cal.set(cal.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
	public static long getCurrentUTCt12() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.HOUR_OF_DAY,  12);
		cal.set(cal.MINUTE, 0);
		cal.set(cal.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
	public static long getCurrentUTCt18() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.HOUR_OF_DAY,  18);
		cal.set(cal.MINUTE, 0);
		cal.set(cal.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
	public static long getCurrentUTCt24() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.HOUR_OF_DAY,  23);
		cal.set(cal.MINUTE, 59);
		cal.set(cal.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
	
	public static String getCurrentUTC() {
		DateFormat utc2 = new SimpleDateFormat("dd MMM yyyy HH:mm z");
		utc2.setTimeZone(utctimezone);
		return utc2.format(new Date(getCurrentUTCtime()));
	}

	public static String getsCurrentUTCtime() {
		DateFormat utc2 = new SimpleDateFormat("HH:mm");
		utc2.setTimeZone(utctimezone);
		return utc2.format(new Date(getCurrentUTCtime()));
	}

	public static String getDefaultTime() {
		DateFormat utc2 = new SimpleDateFormat("HH:mm");
		utc2.setTimeZone(defaulttimezone);
		return utc2.format(new Date(getCurrentUTCtime()));
	}
	public static String getDefaultDateShort() {
		DateFormat utc2 = new SimpleDateFormat("dd MMM");
		utc2.setTimeZone(defaulttimezone);
		return utc2.format(new Date(getCurrentUTCtime()));
	}
	
	public static String getsCurrentUTCdateshort() {
		DateFormat utc2 = new SimpleDateFormat("dd MMM");
		utc2.setTimeZone(utctimezone);
		return utc2.format(new Date(getCurrentUTCtime()));
	}

	public static Date getDateFromDDMMMYY(String s) {
		try {
			DateFormat ddmmyy = new SimpleDateFormat("ddMMMyy");
			Date d = ddmmyy.parse(s);
			return d;
		} catch(Exception e) {
			return null;
		}
	}
	public static String toSQL(Date d) {
		DateFormat ddmmyy = new SimpleDateFormat("yyyy-MM-dd");
		return ddmmyy.format(d);
	}

	public static boolean isToday(long utc) {
		SimpleDateFormat df = new SimpleDateFormat("D");
		String d1 = df.format(new Date(System.currentTimeMillis()));
		String d2 = df.format(new Date(utc));
		return d1.equals(d2);
	}
	/*
	public static TimeZone getMyTimezone() throws Exception {
		return new Incrementer().getMyTimeZone();
	}
	
	public static void refreshTZ() {
		defaulttimezone = new Incrementer().getMyTimeZone();
	}

	/**
	 * Returns the timestamp (long) designating the 0.00H LOCAL TIME on the first day of the pending week 
	 * @return
	 */
	public static long getLocalMD1() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.DAY_OF_WEEK, cal.MONDAY);
		cal.set(cal.HOUR_OF_DAY, 0);
		cal.set(cal.MINUTE, 0);
		cal.set(cal.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	public static long getLocalD1() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.HOUR_OF_DAY, 0);
		cal.set(cal.MINUTE, 0);
		cal.set(cal.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	public static long getLocalMD2() {
		Calendar cal = Calendar.getInstance(defaulttimezone);
		cal.set(cal.DAY_OF_WEEK, cal.MONDAY);
		cal.set(cal.HOUR_OF_DAY,  0);
		cal.set(cal.MINUTE, 0);
		cal.set(cal.MILLISECOND, 0);
		cal.add(cal.DATE,  29);
		return cal.getTimeInMillis();
	}
	
	public static String getT1(long ref) throws Exception {
		long now = getCurrentUTCtime();
		if (ref>now) {
			long diff = ref - now;
			Time t = new Time(diff);
			return " - "+defaulthtime.format(t);
		}
		long diff = now - ref;
		Time t = new Time(diff);
		return " + "+defaulthtime.format(t);
	}
	
	public static String getT1a(long ref) throws Exception {
		long now = getCurrentUTCtime();
		long diff = ref - now;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		if (diffMinutes<0 || diffHours<0) {
			return " + "+String.valueOf(-diffHours)+"h"+String.valueOf(-diffMinutes);
		}
		if (diffHours==0) {
			return " - "+String.valueOf(diffMinutes);
		}
		return " - "+String.valueOf(diffHours)+"h"+String.valueOf(diffMinutes);
	}
	
	public static List<TimeZone>  getTimezonesSorted() {
		final String[] timeZoneIds = TimeZone.getAvailableIDs();
		final List<TimeZone> timeZones = new ArrayList<TimeZone>();
		for (final String id : timeZoneIds) {
		   timeZones.add(TimeZone.getTimeZone(id));
		}
		Collections.sort(timeZones, new Comparator<TimeZone>() {
			   public int compare(final TimeZone a, final TimeZone b) {
				   if (a.getRawOffset()<b.getRawOffset()) return -1;
				   if (a.getRawOffset()>b.getRawOffset()) return 1;
				   return 0;
			   }
			});
		return timeZones;
	}
	

}
