package com.detrics.iatci.edi;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.elements.DMC;
import com.detrics.iatci.edi.elements.FDQ;
import com.detrics.iatci.edi.elements.LOR;
import com.detrics.iatci.edi.elements.PPD;
import com.detrics.iatci.edi.elements.PRD;
import com.detrics.iatci.edi.elements.UNB;
import com.detrics.iatci.edi.elements.UNH;
import com.detrics.iatci.edi.elements.UNT;
import com.detrics.iatci.edi.elements.UNZ;
import com.detrics.iatci.orm.Participation;

/**
 * The Inter Airline Through Checkin Implementation
 * 
 *  example:
 *
 *             sender+recipient+date+time+msgcode
   Example needed
 * 
 * @author natacha
 *
 */
public class DCQCKF  extends DCQ implements Message{
	/**
	 * Constructor for an incoming DCQCKI-request from a third party.
	 * This is then a wrapper around the actual message
	 * @param msgID
	 * @param s
	 * @throws Exception
	 */
	public DCQCKF(int msgID, String s) {
		this.msgID = msgID;
		this.message = s;
	}
	
	/**
	 * Generate the complete message for the given Session.
	 * The Session contains the flight and pax at hand.
	 * A typical DCQCKI-message looks like:
	 *  UNB UNH FDQ {[PPD PRD PSD PBD PSI],..} UNT UNZ
	 *  @param session the given session
	 *  
	 */
	public void generateMessage(Session session) throws Exception {
		isGenerated = false;
		 
		UNB unb = new UNB(session);
		StringBuilder msg = unb.getUNB();
		UNH unh = new UNH("RTZDCS", this, session);
		LOR lor = new LOR(session);
		DMC dmc = new DMC(session);
		FDQ fdq = new FDQ(session);
		msg.append("\n");
		msg.append(unh.getUNH());
		msg.append("\n");
		msg.append(lor.getLOR());
		msg.append("\n");
		msg.append(dmc.getDMC());
		msg.append("\n");
		msg.append(fdq.getFDQ());
		

		
		UNT unt = new UNT(session);
		UNZ unz = new UNZ(session);
		msg.append(unt.getUNT());
		msg.append("\n");
		msg.append(unz.getUNZ());
		msg.append("\n");
		this.message = msg.toString();
		isGenerated = true;
	}
	

	
	/**
	 * Helper-method for constructing outbound message.
	 * Since the population of attributes (needed to generate the message) can be non-atomic (i.e. might be distributed over multiple calls,
	 * since not all attr-values are known initially), this method will check if all mandatory attrs are currently validly populated.
	 *  
	 */
	@Override
	public boolean validateOutbound() {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * Returns the canonical IATCI-code of this message-type 
	 */
	public String getMessageType() {
		return Message.DCQCKI;
	}
	
	/**
	 * Parser for inbound DCQCKI-messages (an incoming iatci-request)
	 */
	public void parse() throws EDIFormatException {
		// TODO Auto-generated method stub
		
	}

}
