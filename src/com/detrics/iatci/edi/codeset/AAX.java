package com.detrics.iatci.edi.codeset;

/**
 * IATCI codeset AAX : Passenger Types
 * 
 * A = adult
 * C = child
 * M = male
 * F = female
 * 
 * Some carriers just use A/C, others only F/M/C
 * 
 * @author martijn
 *
 */
public class AAX implements Codeset {

	public static final String ADULT = "A";
	public static final String CHILD = "C";
	public static final String MALE = "M";
	public static final String FEMALE = "F";
	
	/**
	 * TODO implement carrier-specific logic ?
	 */
	
}
