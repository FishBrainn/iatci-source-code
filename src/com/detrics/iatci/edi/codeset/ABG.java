package com.detrics.iatci.edi.codeset;

import javax.sql.RowSet;

import com.detrics.iatci.Session;


/**
 * IATCI Datadictionary section D.6.23 Codeset ABG - Messages for Level 1
 * 
 * Used in element 9845 - Errors and warnings
 * 
 * This is a mapper against the table iatci_9845, containing all warning/error messages
 * 
 * @author martijn
 *
 */
public class ABG implements Codeset {

	public static String getABG(String c055_9845) {
		String[] parts = c055_9845.split("\\+");
		int idx = Integer.parseInt(parts[0]);
		try {
			RowSet rs = new Session().getQuery("SELECT * FROM iatci_9845 WHERE idx="+idx);
			if (rs.next()) {
				return rs.getString("msg9845");
			}
		} catch(Exception e) {
			//TODO escalate properly
			e.printStackTrace();
		}
		return "ABG_"+c055_9845+" unknown";
	}
	
}
