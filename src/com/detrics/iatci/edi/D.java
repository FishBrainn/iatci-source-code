package com.detrics.iatci.edi;

import java.util.Collection;
import java.util.Vector;

import javax.sql.RowSet;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.elements.WAD;
import com.detrics.iatci.utils.UTCnexus;

public class D {

	public String message;
	public String IO;
	public int msgID;
	public long timestamp;

	public static Message findByPK(int PK) throws Exception {
		RowSet rs = new Session().getQuery("SELECT * FROM iatci_msg WHERE msgID="+PK);
		if (rs.next()) {
			String msg = rs.getString("msg");
			String msgtype = rs.getString("msgtype");
			if (msgtype.equals(Message.DCQCKI)) {
				DCQCKI m = new DCQCKI(PK, msg);
				m.setIO(rs.getString("io"));
				m.setTimestamp(rs.getLong("mts"));
				m.setMsgID(PK);
				return m;
			}
			if (msgtype.equals(Message.DCRCKA)) {
				DCRCKA m = new DCRCKA(PK, msg);
				m.setIO(rs.getString("io"));
				m.setTimestamp(rs.getLong("mts"));
				m.setMsgID(PK);
				return m;
			}
		}
		return null;
	}
	
	public String getIO() {
		return IO;
	}
	public String getIOw() {
		if (this.IO.equals("I")) return "&larr;";
		return "&rarr;";
	}
	public void setIO(String iO) {
		IO = iO;
	}
	public int getMsgID() {
		return msgID;
	}
	public void setMsgID(int msgID) {
		this.msgID = msgID;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public boolean isGenerated() {
		return isGenerated;
	}
	public void setGenerated(boolean isGenerated) {
		this.isGenerated = isGenerated;
	}
	public boolean isHasGenFailed() {
		return hasGenFailed;
	}
	public void setHasGenFailed(boolean hasGenFailed) {
		this.hasGenFailed = hasGenFailed;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String s) {
		this.message = s;
	}
	
	public String getCreatedDate() {
		return UTCnexus.getIATAdate(this.timestamp);
	}
	
	public String getCreatedTime() {
		return UTCnexus.getIATAtime(timestamp);
	}
	
	/**
	 * Boolean to indicate if the message-status indicates that the outbound message
	 * is succesfully generated.
	 * The value 'false' may indicate that either generation has failed because of missing or invalid mandatory attrs,
	 * of that the method generateMessage has not been called yet.
	 * Perhaps these 2 reasons require differentiation.  
	 */
	protected boolean isGenerated = false;
	
	/**
	 * Indicates whether or not the generation of this outbound message has failed.
	 */
	protected boolean hasGenFailed = false;

	public void addWarning(WAD wad) {
		try {
			new Session().insert("INSERT INTO iatci_msglog (msgID, iatci_errID, errtxt) VALUES("+this.msgID+","+wad.get9845()+",'"+wad.get4440()+"')");
		} catch(Exception e) {
			// TODO escalate properly
			e.printStackTrace();
		}
	}

	public Collection<WAD> getWarnings() throws Exception {
		Vector<WAD> list = new Vector<WAD>();
		RowSet rs = new Session().getQuery("SELECT * FROM iatci_msglog WHERE msgID="+this.msgID);
		while (rs.next()) {
			WAD wad = new WAD(rs.getString("iatci_errID"), rs.getString("errtxt"));
			list.add(wad);
		}
		return list;
	}
	
	public void addError(WAD wad) {
		// TODO Auto-generated method stub
		
	}
	
}
