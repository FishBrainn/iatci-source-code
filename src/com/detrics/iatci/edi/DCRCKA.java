package com.detrics.iatci.edi;

import java.sql.Date;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.elements.UNB;
import com.detrics.iatci.edi.elements.UNH;
import com.detrics.iatci.edi.elements.WAD;

public class DCRCKA extends DCR implements Message {

	private int reservationID;
	
	public DCRCKA(int msgID, String inboundmsg) throws EDIFormatException {
		message = inboundmsg;
		this.msgID = msgID;
	}
	
	public DCRCKA() {
		
	}

	public void generateMessage(Session session) throws Exception {
		UNB unb = new UNB(session);
		StringBuilder msg = unb.getUNB();
		UNH unh = new UNH("RTZDCS", this, session);
		msg.append("\n");
		msg.append(unh.getUNH());
		
		this.message = msg.toString();
	}

	@Override
	public String getMessageType() {
		return this.DCRCKA;
	}

	@Override
	public int getMsgID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean validateOutbound() {
		// TODO Auto-generated method stub
		return false;
	}

	public void parse() throws EDIFormatException {
		String[] lines = message.split("'");
		for (int k=0; k<lines.length; k++) {
			String line = lines[k];
			String[] parts = line.split("\\+");
			if (line.startsWith("WAD")) {
				WAD wad = new WAD(line);
				this.addWarning(wad);
			}
		}
	}
	
}
