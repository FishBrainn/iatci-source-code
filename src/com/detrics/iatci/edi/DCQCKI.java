package com.detrics.iatci.edi;

import java.sql.Date;

import javax.sql.RowSet;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.elements.*;
import com.detrics.iatci.orm.Participation;

/**
 * The actual Through Checkin Request
 * 
 *  example:
 *
 *             sender+recipient+date+time+msgcode
    UNB+IATA:1+SK+HUB+110601:0158+09683F'
     
	UNH+RTZDCS+DCQCKI:03:1:IA+09683F' 
	LOR+SK:CPH' 
	DMC+++5'

1	FDQ+UA+5+961201+JFK+LAX++LH+400+9612010400+9612011000+FRA+JFK'
2	FDQ+UA+222+110601+ORD+YYZ+A+SK+111+110601+1106010930+CPH+ORD++A'
	       outb fno    from to     inb fno  inb date-time dept  inb date-time arr      from to
1	FDQ+UA +222+110601 +ORD+YYZ+A  +SK+111   +110601             +1106010930            +CPH+ORD++A' 
2	FDQ+UA +5+961201   +JFK+LAX+   +LH+400   +9612010400         +9612011000            +FRA+JFK'
	
	PPD+BENNETT+M:N++CLEMIELOU++3009050010+1:1' 
	PRD+V+OK++S2ZAJS+564QT3' 
	PSD+N' 
	PBD+0++HP' 
	PSI++TKNE::00578743626401::::I' 
	
	PPD+BENNETT+M:N++BLAINB++3009050011+2:1' 
	PRD+V+OK++S2ZAJS+564QT3' 
	PSD+N' 
	PBD+0++MP' 
	PSI++TKNE::00578743626421::::I' 
	
	UNT+14+RTZDCS' 
	UNZ+1+09683F'
 * 
 * @author martijn
 *
 */
public class DCQCKI extends DCQ implements Message {

	//private String message;
	
	
	public DCQCKI() {
		
	}

	/**
	 * Generate the complete message for the given Session.
	 * The Session contains the flight and pax at hand.
	 * A typical DCQCKI-message looks like:
	 *  UNB UNH FDQ {[PPD PRD PSD PBD PSI],..} UNT UNZ
	 *  @param session the given session
	 *  
	 */
	public void generateMessage(Session session) throws Exception {
		isGenerated = false;
		 
		UNB unb = new UNB(session);
		StringBuilder msg = unb.getUNB();
		UNH unh = new UNH("RTZDCS", this, session);
		LOR lor = new LOR(session);
		DMC dmc = new DMC(session);
		FDQ fdq = new FDQ(session);
		msg.append("\n");
		msg.append(unh.getUNH());
		msg.append("\n");
		msg.append(lor.getLOR());
		msg.append("\n");
		msg.append(dmc.getDMC());
		msg.append("\n");
		msg.append(fdq.getFDQ());
		
		int f = 0;
		for (Participation p : session.getPax()) {
			msg.append(this.getPaxPart(p, ++f, 1));
			msg.append("\n");
		}
		
		UNT unt = new UNT(session);
		UNZ unz = new UNZ(session);
		msg.append(unt.getUNT());
		msg.append("\n");
		msg.append(unz.getUNZ());
		msg.append("\n");
		this.message = msg.toString();
		isGenerated = true;
	}

	/**
	 * Generates and returns the section with the pax-details
	 * @param part
	 * @param familyconnector
	 * @param paxnr
	 * @return
	 */
	private StringBuilder getPaxPart(Participation part, int familyconnector, int paxnr) {
		StringBuilder msg = new StringBuilder();
		PPD ppd = new PPD(part, familyconnector, paxnr);
		msg.append(ppd.getPPD());
		msg.append("\n");
		PRD prd = new PRD(part);
		msg.append(prd.getPRD());
		msg.append("\n");
		return msg;
	}
	
	/**
	 * Constructor for an incoming DCQCKI-request from a third party.
	 * This is then a wrapper around the actual message
	 * @param msgID
	 * @param s
	 * @throws Exception
	 */
	public DCQCKI(int msgID, String s) {
		this.msgID = msgID;
		this.message = s;
	}
	
	/**
	 * Returns the canonical IATCI-code of this message-type 
	 */
	public String getMessageType() {
		return Message.DCQCKI;
	}

	@Override
	public int getMsgID() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Helper-method for constructing outbound message.
	 * Since the population of attributes (needed to generate the message) can be non-atomic (i.e. might be distributed over multiple calls,
	 * since not all attr-values are known initially), this method will check if all mandatory attrs are currently validly populated.
	 *  
	 */
	@Override
	public boolean validateOutbound() {
		/* TODO implement validation (population of all mandatory fields */
		return true;
	}

	/**
	 * Parser for inbound DCQCKI-messages (an incoming iatci-request)
	 */
	@Override
	public void parse() {
		// TODO implement a parser, probably separate class-structure, for inbound messages
		
	}


}
