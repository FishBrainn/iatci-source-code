package com.detrics.iatci.edi.elements;

import java.sql.Date;

import com.detrics.iatci.Session;
import com.detrics.iatci.edi.EDIFormatException;
import com.detrics.iatci.utils.UTCnexus;

/**
 * IATCI Datadicationary element
 * 
 * Location of Request
 * 
 * Sample:
 * 
 * LOR+SK:CPH'
 * LOR + airline-designator : place-of-orgin
 * 
 * @author martijn
 *
 */
public class LOR extends EDIelement {

	private String CO59_3127;		// airline designator  (an..17)
	private String CO59_3800;		// place of origin of business (a..5)
	private String CO59_3805; 	// country of origin of business (an..2)
	
	/**
	 * Constructor, taking all dynamic elements
	 * Checks on validity of given params (mandatory, length, etc)
	 * @throws EDIFormatException if a parameter does not comply to syntax
	 */
	public LOR(Session session) throws EDIFormatException{
		CO59_3127 = session.getISender().getAirliner2();
		CO59_3800 = session.getISender().getStation3();
	}

	public StringBuilder getLOR() {
		StringBuilder lor = new StringBuilder("LOR");
		lor.append(PLUS);
		lor.append(CO59_3127);
		lor.append(COLON);		
		lor.append(CO59_3800);
		lor.append(EOL);
		return lor;
	}
	
}
