package com.detrics.iatci.edi.elements;
/**
 * IATCI messaging
 * 
 * CHD Cascading Host Details
 * 
 * Sample:
 * CHD+SK:CPH’
 * 
 * Section D.5.4 in IATCI DD
 * 
 * @author natacha
 *
 */
public class CHD extends EDIelement{
	public CHD(){
		
	}
	public StringBuilder getCHD() {
		StringBuilder chd = new StringBuilder("CHD");
		chd.append(PLUS);
		

		
		chd.append(EOL);
		
		return chd;
	}
}
