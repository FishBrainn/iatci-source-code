package com.detrics.iatci.edi.elements;

import com.detrics.iatci.edi.codeset.AAI;
import com.detrics.iatci.edi.codeset.AAX;
import com.detrics.iatci.orm.Participation;

/**
 * IATCI messaging
 * 
 * PSD passenger seat request details
 * 
 * Sample:
 * PSD+N:W'
 * PSD+++22:3:D:E'
 * 
 * Section D.5.21 in IATCI DD
 * 
 * @author natacha
 *
 */

public class PSD extends EDIelement {
	//-----
	private String C024_9807; //M seating preference indicator
	private String C024_9825; //C seat characteristics
	
	private String S9809; //C specific seat request
	
	private String C025_9830; //C Seat row number first row
	private String C025_6804; //C number of rows
	private String C025_9831; //C seat column
	
	
	/**
	 * Default constructor, will initialize fields with details about the requested seat
	 * @param seatPreferenceIndicator 
	 * @param seatCharacteristics 
	 * @param specificSeatRequest
	 * @param seatRowNumber
	 * @param numberOfRows
	 * @param seatColumn
	 * 
	 */
	public PSD(String seatPreferenceIndicator, String seatCharacteristics, String specificSeatRequest, int seatRowNumber, int numberOfRows, int seatColumn) {
		C024_9807 = seatPreferenceIndicator;
		C024_9825 = seatCharacteristics;
		S9809 = specificSeatRequest;
		C025_9830 = String.valueOf(seatRowNumber);
		C025_6804 = String.valueOf(numberOfRows);
		C025_9831 = String.valueOf(seatColumn);
	}

	/**
	 * Generates the PSD-element
	 * @return
	 */
	public StringBuilder getPSD() {
		StringBuilder psd = new StringBuilder("PSD");
		psd.append(PLUS);
		psd.append(C024_9807);
		psd.append(COLON);
		psd.append(C024_9825);
		
		psd.append(PLUS);
		psd.append(S9809);
		
		psd.append(PLUS);
		psd.append(C025_9830);
		psd.append(COLON);
		psd.append(C025_6804);
		psd.append(COLON);
		psd.append(C025_9831);
		
		psd.append(EOL);
		
		return psd;
	}

	
	
	
}
