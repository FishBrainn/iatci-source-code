package com.detrics.iatci.edi.elements;

import com.detrics.iatci.orm.Participation;

/**
 * IATCI messaging
 * 
 * PSI passenger service information
 * 
 * Sample:
 * PPSI+++DEAF:BLND'
 * PSI++TKNE::07221001234562+TKNE::INF07221001234572’ (sample of an adult with an infant)
 * 
 * Section D.5.22 in IATCI DD
 * 
 * @author natacha
 *
 */

public class PSI extends EDIelement {
	private String S9838; // C OSI details
	
	//API PASSENGER DETAILS
	private String C697_9916; //C Date of birth
	private String C697_1004; //C Passport number
	private String C697_3207; //C Nationality
	private String C697_3809; //C API first name version
	private String C697_3808; //C API surname version
	private String C697_9869; //C Processing status
	private String C697_4440; //C Free text
	
	//SSR DETAILS
	private String C030_9837; //M SSR code
	private String C030_3127; //C Airline designator
	private String C030_9839; //C SSR additional information
	private String C030_9886; //C Age of passenger
	private String C030_6806; //C Number of pieces of SSR items/tier level for FQTV
	private String C030_6803; //C Weight combined of all SSR items
	private String C030_4440; //C Free text
	private String C030_1001; //C Infant ticket
	
	private String iatci_version;
	
	
	/**
	 * Default constructor, will initialize fields with person-details from given participation as well as specific information about the passenger and about SSR. Not all paramaters are required, as only the 94:1 version uses passenger information for example.
	 * @param versionIatci
	 * @param OSIDetails
	 * @param part the participation containing the person
	 * @param dateOfBirth of the passenger
	 * @param passportNumber of the passenger
	 * @param nationality of the passenger
	 * @param processingStatus
	 * @param passengerFreeText some free text space for the passenger part
	 * @param SSRcode
	 * @param airlinerDesignator
	 * @param SSRextraInfo SSR additional information
	 * @param ageOfPassenger
	 * @param numberOfSSRitems Number of pieces of SSR items/tier level for FQTV
	 * @param weightOfItems Weight combined of all SSR items
	 * @param SSRfreeText some free text space for the SSR part
	 * @param infantETKT Infant ETKT number
	 * 
	 */
	public PSI(String versionIatci, String OSIDetails, Participation part, String dateOfBirth, String passportNumber, String nationality, String processingStatus, String passengerFreeText, String SSRCode, String airlinerDesignator, String SSRextraInfo, int ageOfPassenger, int numberOfSSRitems, int weightOfItems, String SSRfreeText, String infantETKT)
	{
		iatci_version = versionIatci;
		S9838 = OSIDetails;
		
		C030_9837 = SSRCode;
		C030_3127 = airlinerDesignator;
		C030_9839 = SSRextraInfo;
		C030_9886 = String.valueOf(ageOfPassenger);
		C030_6806 = String.valueOf(numberOfSSRitems);
		C030_6803 = String.valueOf(weightOfItems);
		C030_4440 = SSRfreeText;
		
		if(iatci_version == "94:1")
		{
			C697_9916 = dateOfBirth;
			C697_1004 = passportNumber;
			C697_3207 = nationality;
			C697_3809 = part.getPaxfirstname();
			C697_3808 = part.getPaxname();
			C697_9869 = processingStatus;
			C697_4440 = passengerFreeText;
		}
		else if (iatci_version == "07:1")
		{
			C030_1001 = infantETKT;
		}
	}
	
	/**
	 * Generates the PSI-element
	 * @return
	 */
	public StringBuilder getPSI() {
		StringBuilder psi = new StringBuilder("PSI");
		
		if(iatci_version == "94:1")
		{
			psi.append(PLUS);
			psi.append(S9838);
			
			psi.append(PLUS);
			psi.append(C697_9916);
			psi.append(COLON);
			psi.append(C697_1004);
			psi.append(COLON);
			psi.append(C697_3207);
			psi.append(COLON);
			psi.append(C697_3809);
			psi.append(COLON);
			psi.append(C697_3808);
			psi.append(COLON);
			psi.append(C697_9869);
			psi.append(COLON);
			psi.append(C697_4440);
			
			psi.append(PLUS);
			psi.append(C030_9837);
			psi.append(COLON);
			psi.append(C030_3127);
			psi.append(COLON);
			psi.append(C030_9839);
			psi.append(COLON);
			psi.append(C030_9886);
			psi.append(COLON);
			psi.append(C030_6806);
			psi.append(COLON);
			psi.append(C030_6803);
			psi.append(COLON);
			psi.append(C030_4440);
		}
		else if (iatci_version == "07:1")
		{
			psi.append(PLUS);
			psi.append(S9838);
			
			psi.append(PLUS);
			psi.append(C030_9837);
			psi.append(COLON);
			psi.append(C030_3127);
			psi.append(COLON);
			psi.append(C030_9839);
			psi.append(COLON);
			psi.append(C030_9886);
			psi.append(COLON);
			psi.append(C030_6806);
			psi.append(COLON);
			psi.append(C030_6803);
			psi.append(COLON);
			psi.append(C030_4440);
			psi.append(COLON);
			psi.append(C030_1001);
		}
		else
		{
			psi.append(PLUS);
			psi.append(S9838);
			
			psi.append(PLUS);
			psi.append(C030_9837);
			psi.append(COLON);
			psi.append(C030_3127);
			psi.append(COLON);
			psi.append(C030_9839);
			psi.append(COLON);
			psi.append(C030_9886);
			psi.append(COLON);
			psi.append(C030_6806);
			psi.append(COLON);
			psi.append(C030_6803);
			psi.append(COLON);
			psi.append(C030_4440);

		}
		
		return psi;
	}
	
}
