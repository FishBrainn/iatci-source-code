package com.detrics.iatci.edi.elements;

import java.sql.Date;

import com.detrics.iatci.Session;
import com.detrics.iatci.edi.EDIFormatException;
import com.detrics.iatci.utils.UTCnexus;

/**
 * IATCI messaging
 * 
 * United Nations Service Segments
 * UNB Interchange header
 * 
 * First element in all messages
 * 
 * Sample:
 * UNB+IATA:1+SK+HUB+110601:0158+09683F'
 * UNB +syntax + sender + recipient +  date + time + sessionID
 * 
 * Section D.4.1 in IATCI DD
 * 
 * @author martijn
 *
 */
public class UNB extends EDIelement {

	/**
	 * Syntax id IATA or UNOA
	 */
	private String S1_1;		// Syntax id	- still often UNOA:1 is used (but is deprecated)
	private String S1_2;		// Syntax version nr
	private String S2_4;		// Sender identification - airline code
	private String S2_7;		// Partner ID code qualifier, optional
	private String S2_8;		// Address for reverse routing, optional
	private String S3_10;		// Recipient ID - airline code
	private String S3_7e;		// Ident code qualifier, optional
	private String S3_14;		// Routing addr, optional
	private String S4_17;		// Date
	private String S4_19;		// Time
	private String S_20;		// Interchange control ref
	
	
	/**
	 * Constructor, taking all dynamic elements
	 * Checks on validity of given params (mandatory, length, etc)
	 * @throws EDIFormatException if a parameter does not comply to syntax
	 */
	public UNB(Session session) throws EDIFormatException{
		S1_1 = "IATA";
		S1_2 = "1";
		S2_4 = session.getISender().getAirliner2();
		S3_10 = session.getiRecipient().getAirliner2();
		if (S3_10.length()==0) S3_10 = session.getiRecipient().getAirliner3();
		S4_17 = UTCnexus.getIATAdate(session.getTS());
		S4_19 = UTCnexus.getIATAtime(session.getTS());
		S_20= session.getSessionID();
	}
	
	/**
	 * Concatenates all elements, and returns the full UNB-element as a StringBuilder
	 * 
	 * @return
	 */
	public StringBuilder getUNB() {
		StringBuilder unb = new StringBuilder("UNB");
		unb.append(PLUS);
		
		unb.append(S1_1).append(COLON).append(S1_2);
		unb.append(PLUS);
		
		unb.append(S2_4);
		if (S2_7!=null) {
			unb.append(COLON).append(S2_7).append(COLON).append(S2_8);
		}
		unb.append(PLUS);
		
		unb.append(S3_10);
		if (S3_7e!=null) {
			unb.append(COLON).append(S3_7e).append(S3_14);
		}
		unb.append(PLUS);
		
		unb.append(S4_17).append(COLON).append(S4_19);
		unb.append(PLUS);
		
		unb.append(S_20);
		unb.append(EOL);
		
		return unb;
	}
	
	
	
	/**
	 * Constructor for existing UNB, parses all elements, so that they are eligable for getters
	 * @param s
	 */
	public UNB(String s) {
		String[] parts = s.split("+");
		// etc
	}

	public String getS1_1() {
		return S1_1;
	}

	public String getS1_2() {
		return S1_2;
	}

	public String getS2_4() {
		return S2_4;
	}

	public String getS2_7() {
		return S2_7;
	}

	public String getS2_8() {
		return S2_8;
	}

	public String getS3_10() {
		return S3_10;
	}

	public String getS3_7e() {
		return S3_7e;
	}

	public String getS3_14() {
		return S3_14;
	}

	public String getS4_17() {
		return S4_17;
	}

	public String getS4_19() {
		return S4_19;
	}

	public String getS_20() {
		// this is  test
		return S_20;
	}
	
}
