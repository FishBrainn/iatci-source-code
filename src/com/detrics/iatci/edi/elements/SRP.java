package com.detrics.iatci.edi.elements;
/**
 * IATCI messaging
 * 
 * SRP Select Request Parameters
 * 
 * Sample:
 * SRP+L:N'.
 * 
 * Section D.5.26 in IATCI DD
 * 
 * @author natacha
 *
 */
public class SRP extends EDIelement{

	public SRP(){
		
	}
	public StringBuilder getSRP() {
		StringBuilder srp = new StringBuilder("SRP");
		srp.append(PLUS);
		

		
		srp.append(EOL);
		
		return srp;
	}
}
