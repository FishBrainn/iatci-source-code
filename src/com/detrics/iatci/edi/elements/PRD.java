package com.detrics.iatci.edi.elements;

import com.detrics.iatci.orm.Participation;

/**
 * IATCI messaging
 * 
 * PRD passenger reservation details
 * 
 * Sample:
 * PRD+V+OK++S2ZAJS+564QT3'
 * 
 * Section D.5.20 in IATCI DD
 * 
 * @author martijn
 *
 */
public class PRD extends EDIelement {

	private String SC023 = "";		// class information (M)
	private String C023_9800 = "";	// reservations/booking desginator (M)
	
	private String S9822 = "OK";	// res status (default OK) (M)
	
	private String SC020 = "";		// PAX PARTY REF (C)
	private String  C020_9826 = "";	// alpha party ref (M)
	private String  C020_9827 = "";	// num party ref (M)
	
	private String S9823_Q = "";	// recloc querying system (C)
	private String S9823_R = "";	// recloc responding system (C)
	
	private String SC022 = "";		// PNR details booking system (M)
	private String  SC022_3217 = "";	// carrier code booking system (M) an..17
	private String  SC022_9823 = "";	// pax recloc booking system (M) an..9
	
	public PRD(Participation p) {
		SC023 = p.getSeatclass();
		C023_9800 = p.getRecloc();
		S9822 = "OK";
		
	}
	
	public StringBuilder getPRD() {
		StringBuilder prd = new StringBuilder("PRD");
		
		prd.append(PLUS);
		prd.append(SC023);
		prd.append(PLUS);
		prd.append(S9822);
		prd.append(PLUS);
		prd.append(PLUS);
		prd.append(C023_9800);
		prd.append(PLUS);
		prd.append(SC020);
		
		
		prd.append(EOL);
		return prd;
	}
	
}
