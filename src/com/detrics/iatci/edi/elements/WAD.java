package com.detrics.iatci.edi.elements;

import com.detrics.iatci.Session;
import com.detrics.iatci.edi.EDIFormatException;
import com.detrics.iatci.edi.codeset.ABG;

/**
 * IATCI Datadicationary element
 * D.5.35 WAD Warning Details
 * 
 * Sample:
 * 
 * WAD+1:79'
 * WAD+1:1'
 * WAD+1::Boarding pass at Gate'
 * WAD+1:193+1:258:NO API+1:259+1:260+1:262'
 * 
 * @author martijn
 *
 */
public class WAD extends EDIelement {

	private String C055 = "";		// M passenger surname
	private String C055_9876;		// Processing level n1 M AAZ-codeset
	private String C055_9845;		// Msg number n..7 C ABG-codeset
	private String C055_4440;		// Warning msg free text an..70

	/**
	 * Outbound constructor
	 * @param session
	 * @throws EDIFormatException
	 */
	public WAD(Session session) throws EDIFormatException{
	}
	
	public WAD(String s9845, String s4440) {
		this.C055_4440 = s4440;
		this.C055_9845 = s9845;
	}
	
	/**
	 * Inboud constructor
	 * Expects string in form of WAD+[level]:[msg nr]:[custom message (optional)]
	 * 
	 * If the custom message is not present, it will fetch from ABG-codeset
	 * 
	 * @param s
	 * @throws EDIFormatException
	 */
	public WAD(String s) throws EDIFormatException{
		String[] parts = s.split("\\+");
		String[] parts2= parts[1].split(":");
		this.C055_9876 = parts2[0];
		this.C055_9845 = parts2[1];
		if (parts2.length>2) {
			this.C055_4440 = parts2[2];
		} else {
			this.C055_4440 = ABG.getABG(C055_9845);
		}
	}

	public String get9845() throws Exception {
		return C055_9845;
	}
	
	public String get4440() throws Exception {
		return C055_4440;
	}
	
}
