package com.detrics.iatci.edi.elements;

import com.detrics.iatci.Session;

public class UNZ extends EDIelement {

	private String sessionID;
	
	public UNZ(Session session) {
		this.sessionID = session.getSessionID();
	}
	
	public StringBuilder getUNZ() {
		StringBuilder unt = new StringBuilder("UNZ");
		unt.append(PLUS);
		unt.append(sessionID);
		unt.append(EOL);
		return unt;
	}

}
