package com.detrics.iatci.edi.elements;

import com.detrics.iatci.Session;
import com.detrics.iatci.edi.EDIFormatException;

/**
 * IATCI Datadicationary element
 * D.5.7 DMC Default Message Characteristics
 * 
 * Sample:
 * 
 * DMC+++5'
 * 
 * 
 * @author martijn
 *
 */
public class DMC extends EDIelement {

	private String maxflights = "5";
	
	public DMC(Session session) throws EDIFormatException{
	}

	public StringBuilder getDMC() {
		StringBuilder dmc = new StringBuilder("DMC");
		dmc.append(PLUS);
		dmc.append(PLUS);
		dmc.append(PLUS);
		dmc.append(maxflights);
		dmc.append(EOL);
		return dmc;
	}
	
	
}
