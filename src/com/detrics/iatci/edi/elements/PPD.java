package com.detrics.iatci.edi.elements;

import com.detrics.iatci.edi.codeset.AAI;
import com.detrics.iatci.edi.codeset.AAX;
import com.detrics.iatci.orm.Participation;

/**
 * IATCI messaging
 * 
 * PPD passenger personal details
 * 
 * Sample:
 * PPD+BENNETT+M:N++CLEMILOU++3009050010+1:1'
 * 
 * Section D.5.19 in IATCI DD
 * 
 * @author martijn
 *
 */
public class PPD extends EDIelement {

	private String S3808 = "";		// M passenger surname
	private String C017_9819;		// M Passenger Type
	private String C017_9884 = AAI.NO;		// C Pax with infant indicator, default NO
	
	private String C692_9821;		// M PaxID in responding system (URP)
	private String C692_9821_2;	// C infant ID in responding system (URP)
	
	private String S3809;				// C pax initials/first name
	private String C018_3808;		// C infant surname if different
	private String C018_3809;		// C given infant name
	
	private String C690_9821;		// M paxID in querying system
	private String C690_9821_2;		// C infant ID in queyring system
	
	private String C021_9828;		// M Surname/family name connector
	private String C021_9829;		// M Pax-nr within connection
	
	private String C691_9821;		// M  PAX ID Details in 3rd-party system
	private String C691_9821_2;		// C Infant ID Details in 3rd-party system
	
	/**
	 * Default constructor, will initialize fields with person-details from given participation
	 * @param part the participation containing the person (first part of PPD-element)
	 * @param familyconnector C021_9828 surname/family name connector
	 * @param paxnr C021_9829 pax-nr within connection
	 */
	public PPD(Participation part, int familyconnector, int paxnr) {
		S3808 = part.getPaxname();
		S3809 = part.getPaxfirstname();
		C017_9819 = AAX.ADULT;
		if (part.getGender().equals("M")) C017_9819 = AAX.MALE;
		if (part.getGender().equals("F")) C017_9819 = AAX.FEMALE;
		if (part.getGender().equals("C")) C017_9819 = AAX.CHILD;
		if (part.hasInfant()) C017_9884 = AAI.YES;		
		C692_9821 = part.getPaxIDrespondingSystem();
		C690_9821 = part.getPaxIDqueryingSystem();
		C691_9821 = part.getPaxID3rdSystem();
		C021_9828 = String.valueOf(familyconnector);
		C021_9829 = String.valueOf(paxnr);
	}

	/**
	 * Generates the PPD-element
	 * @return
	 */
	public StringBuilder getPPD() {
		StringBuilder unh = new StringBuilder("PPD");
		unh.append(PLUS);
		
		unh.append(S3808);
		unh.append(PLUS);
		unh.append(C017_9819);
		unh.append(COLON);
		unh.append(C017_9884);
		
		unh.append(PLUS);
		unh.append(C692_9821);
		unh.append(PLUS);
		
		unh.append(S3809);
		unh.append(PLUS);
		unh.append(C690_9821);
		unh.append(PLUS);
		
		unh.append(C021_9828);
		unh.append(COLON);
		unh.append(C021_9829);
		
		unh.append(EOL);
		
		return unh;
	}

	
}
