package com.detrics.iatci.edi.elements;

import java.sql.Date;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.EDIFormatException;
import com.detrics.iatci.utils.UTCnexus;

/**
 * IATCI messaging
 * 
 * United Nations Service Segments
 * UNH message header
 * 
 * First element in all messages
 * 
 * Sample:
 * 
 * UNH+RTZDCS+DCQCKI:03:1:IA+09683F'
 * UNH + msgref + msgtype:version:release:controllingagency + sessionID
 * 
 * Section D.4.2 in IATCI DD
 * 
 * @author martijn
 *
 */
public class UNH extends EDIelement {

	private String S62;			// Msg reference
	private String S9_65;		// Msg type
	private String S9_52="03";		// Msg version nr
	private String S9_54="1";		// Msg release nr
	private String S9_51="IA";		// Controlling agency
	private String S9_57;		// Association assgnd code
	private String S_68;		// Comman Access ref (conditional)
	private String S10_70;	// Status of transfer - Seq of transfer (unused)
	private String S10_73;	// Status of transfer - first and last transfer	(unused)
	
	
	/**
	 * Constructor, taking all dynamic elements
	 * Checks on validity of given params (mandatory, length, etc)
	 * @throws EDIFormatException if a parameter does not comply to syntax
	 */
	public UNH(String ref, Message msg, Session session) throws EDIFormatException{
		this.S62 = ref;
		this.S9_65 = msg.getMessageType();
		this.S_68 = session.getSessionID();
	}
	
	/**
	 * Concatenates all elements, and returns the full UNB-element as a StringBuilder
	 * 
	 * @return
	 */
	public StringBuilder getUNH() {
		StringBuilder unh = new StringBuilder("UNH");
		unh.append(PLUS);
		
		unh.append(S62);
		unh.append(PLUS);
		
		unh.append(S9_65).append(COLON).append(S9_52).append(COLON).append(S9_54).append(COLON).append(S9_51);
		unh.append(PLUS);
		
		unh.append(S_68);
		
		unh.append(EOL);
		
		return unh;
	}
	
	
	
	/**
	 * Constructor for existing UNH, parses all elements, so that they are eligible for getters
	 * @param s
	 */
	public UNH(String s) {
		String[] parts = s.split("+");
		// etc
	}

}
