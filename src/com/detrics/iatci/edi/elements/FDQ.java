package com.detrics.iatci.edi.elements;

import com.detrics.iatci.Session;

/**
 * IATCI Datadicationary element
 * D.5.11 FDQ Flight Details Query
 * 
 * @author martijn
 *
 */
public class FDQ extends EDIelement {

	// outbound carrier
	private String CO13_3127;			// outbound carrier - scheduled carrier code	M
	private String CO13_3127_2;			// outbound carrier - operating carrier code	C
	
	// outbound flightno
	private String CO14_3802;			// outbound flight-no	-	flightnr		M
	private String CO14_9801;			// outbound flight-no	-	operational suffix	C
	private String CO14_2281;			// outb local date/time of dept
	private String CO14_3215;			// outb airport of dept
	private String CO14_3259;			// outb airport/city of arrival a..5
	private String CO14_9856 = "A";		// outb flight continuation indicator

	// inbound carrier
	private String CO15_3127;			// inb carrier scheduled code	M
	private String CO15_3127_2;			// inb carrier operating code	C
	
	// inbound fightno
	private String CO16_3802;			// inbound flight-no	-	flightnr		M
	private String CO16_9801;			// inbound flight-no	-	operational suffix	C
	private String CO16_2281;			// inb local date/time of dept
	private String CO16_2107;			// inb local date/time of arrival
	private String CO16_3215;			// inb airport of dept
	private String CO16_3259;			// inb airport/city of arrival a..5
	private String CO16_9857= "A";		// inb type of sercie indicator
	
	// for version 03:1/04:2/07:1
	private String C310_9916;			// first date
	private String C310_9918;			// first time
	private String C310_9920;			// second date
	private String C310_9922;			// second time
	
	
	public FDQ(Session session) {
		 CO13_3127 = session.getConnectionFlight().getAirliner();
		 CO14_3802 = session.getConnectionFlight().getFlightno();
		 CO14_9801 = "";
		 CO14_2281 = session.getConnectionFlight().getETD();
		 CO14_3215 = session.getConnectionFlight().getDept().getStation3();
		 CO14_3259 = session.getConnectionFlight().getArr().getStation3();

		 CO15_3127 = session.getOriginFlight().getAirliner();
		 CO15_3127_2 = "";
		
		 CO16_3802 = session.getOriginFlight().getFlightno();
		 CO16_9801 = "";
		 CO16_2281 = session.getOriginFlight().getETD();
		 CO16_2107 = session.getOriginFlight().getETA();
		 CO16_3215 = session.getOriginFlight().getDept().getStation3();
		 CO16_3259 = session.getOriginFlight().getArr().getStation3();
		 //CO16_9857 = "";
	}
	
	public StringBuilder getFDQ() {
		StringBuilder fdq = new StringBuilder("FDQ");
		
		// 
		fdq.append(PLUS);
		fdq.append(CO13_3127);
		fdq.append(PLUS);
		
		fdq.append(CO14_3802);
		fdq.append(PLUS);
		fdq.append(CO14_2281);
		fdq.append(PLUS);
		fdq.append(CO14_3215);
		fdq.append(PLUS);
		fdq.append(CO14_3259);
		fdq.append(PLUS);
		fdq.append(CO14_9856);
		fdq.append(PLUS);
		
		fdq.append(CO15_3127);
		fdq.append(PLUS);
		
		fdq.append(CO16_3802);
		fdq.append(PLUS);
		fdq.append(CO16_2281);
		fdq.append(PLUS);
		fdq.append(CO16_2107);
		fdq.append(PLUS);
		fdq.append(CO16_3215);
		fdq.append(PLUS);
		fdq.append(CO16_3259);
		fdq.append(PLUS);
		fdq.append(PLUS);
		fdq.append(CO16_9857);
		fdq.append(EOL);
		return fdq;
	}
	
}
