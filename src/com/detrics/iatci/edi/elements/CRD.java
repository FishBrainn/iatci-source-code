package com.detrics.iatci.edi.elements;

/**
 * IATCI messaging
 * 
 * CRD cancel request details
 * 
 * Sample:
 * CRD+++O’
 * 
 * Section D.5.5 in IATCI DD
 * 
 * @author natacha
 *
 */
public class CRD extends EDIelement {
	private String S9859; //C reason for cancel coded
	private String S9860; //C type of cancel
	
	/**
	 * Default constructor, will initialize fields with the reason and type of cancel
	 * @param cancelReason reason for cancel coded
	 * @param cancelType type of cancel
	 */
	public CRD(String cancelReason, String cancelType)
	{
		S9859 = cancelReason;
		S9860 = cancelType;
	}
	
	public StringBuilder getCRD() {
		StringBuilder crd = new StringBuilder("CRD");
		
		crd.append(PLUS);
		crd.append(S9859);
		
		crd.append(PLUS);
		
		crd.append(PLUS);
		crd.append(S9860);

		crd.append(EOL);
		return crd;
	}
}
