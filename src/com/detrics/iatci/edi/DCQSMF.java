package com.detrics.iatci.edi;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.elements.CHD;
import com.detrics.iatci.edi.elements.DMC;
import com.detrics.iatci.edi.elements.FDQ;
import com.detrics.iatci.edi.elements.LOR;
import com.detrics.iatci.edi.elements.PPD;
import com.detrics.iatci.edi.elements.PRD;
import com.detrics.iatci.edi.elements.SRP;
import com.detrics.iatci.edi.elements.UNB;
import com.detrics.iatci.edi.elements.UNH;
import com.detrics.iatci.edi.elements.UNT;
import com.detrics.iatci.edi.elements.UNZ;
import com.detrics.iatci.orm.Participation;

/**
 * The actual Seat Map Function Request
 * 
 *  example:
 *
 *             sender+recipient+date+time+msgcode
   	UNH+1+DCQSMF:03:1:IA+GUMIKF3LPL0051'&
	LOR+QF:SYD'&
	FDQ+AA+2430+120119+LAX+DFW+T'&
	SRP+F'&
	UNT+5+1'&
	
 * 
 * @author natacha
 *
 */
public class DCQSMF extends DCQ implements Message{

	
	public DCQSMF() {
		
	}
	/**
	 * Generate the complete message for the given Session.
	 * The Session contains the flight and pax at hand.
	 * A typical DCQSMF-message looks like:
	 *  UNB UNH LOR FDQ SRP CHD UNT UNZ
	 *  @param session the given session
	 *  
	 */
	public void generateMessage(Session session) throws Exception {
		isGenerated = false;
		 
		UNB unb = new UNB(session);
		StringBuilder msg = unb.getUNB();
		UNH unh = new UNH("RTZDCS", this, session);
		LOR lor = new LOR(session);
		DMC dmc = new DMC(session);
		FDQ fdq = new FDQ(session);
		SRP srp = new SRP();
		CHD chd = new CHD();
		msg.append("\n");
		msg.append(unh.getUNH());
		msg.append("\n");
		msg.append(lor.getLOR());
		msg.append("\n");
		msg.append(fdq.getFDQ());
		msg.append("\n");
		msg.append(srp.getSRP());
		msg.append("\n");
		msg.append(chd.getCHD());
		
		

		
		UNT unt = new UNT(session);
		UNZ unz = new UNZ(session);
		msg.append(unt.getUNT());
		msg.append("\n");
		msg.append(unz.getUNZ());
		msg.append("\n");
		this.message = msg.toString();
		isGenerated = true;
	}


	
	/**
	 * Constructor for an incoming DCQSMF-request from a third party.
	 * This is then a wrapper around the actual message
	 * @param msgID
	 * @param s
	 * @throws Exception
	 */
	public DCQSMF(int msgID, String s) {
		this.msgID = msgID;
		this.message = s;
	}
	
	/**
	 * Returns the canonical IATCI-code of this message-type 
	 */
	public String getMessageType() {
		return Message.DCQCKI;
	}

	@Override
	public int getMsgID() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Helper-method for constructing outbound message.
	 * Since the population of attributes (needed to generate the message) can be non-atomic (i.e. might be distributed over multiple calls,
	 * since not all attr-values are known initially), this method will check if all mandatory attrs are currently validly populated.
	 *  
	 */
	@Override
	public boolean validateOutbound() {
		/* TODO implement validation (population of all mandatory fields */
		return true;
	}

	/**
	 * Parser for inbound DCQSMF-messages (an incoming iatci-request)
	 */
	@Override
	public void parse() {
		// TODO implement a parser, probably separate class-structure, for inbound messages
		
	}



}
