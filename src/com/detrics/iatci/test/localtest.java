package com.detrics.iatci.test;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import com.detrics.iatci.utils.UTCnexus;

public class localtest {

	private static TimeZone utctimezone = TimeZone.getTimeZone("UTC");

	public static void main(String[] args) {
		localtest t = new localtest();
		t.doit();
	}

	public void doit() {
		System.out.println(getDateTime(35, "0400"));
	}
	
	public String getDateTime(int dayofyear, String rlotime) {
		try {
			SimpleDateFormat t = new SimpleDateFormat("yyyy D:HHmm");
			Date dy = new Date(System.currentTimeMillis());
			SimpleDateFormat sy = new SimpleDateFormat("yyyy");
			SimpleDateFormat sd = new SimpleDateFormat("D");
			String syear = sy.format(dy);
			String sday = sd.format(dy);
			int year = Integer.parseInt(syear);
			int doy =Integer.parseInt(sday);
			System.out.println(dayofyear);
			System.out.println(doy);
			if (dayofyear<doy) year++;
			t.setTimeZone(utctimezone);
			java.util.Date d = t.parse(String.valueOf(year)+" "+String.valueOf(dayofyear)+":"+rlotime);
			long s = d.getTime();
			Date d2 = new Date(s);
			DateFormat fdqtime = new SimpleDateFormat("yyMMddHHmm");
			fdqtime.setTimeZone(utctimezone);
			return fdqtime.format(d2);
		} catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}
