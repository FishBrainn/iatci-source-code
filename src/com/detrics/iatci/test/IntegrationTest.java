package com.detrics.iatci.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.edi.DCQCKI;
import com.detrics.iatci.edi.elements.UNB;
import com.detrics.iatci.edi.elements.UNH;
import com.detrics.iatci.utils.UTCnexus;

/**
 * Servlet implementation class IntegrationTest
 */
@WebServlet("/IntegrationTest")
public class IntegrationTest extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String iatcisessionID = "";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IntegrationTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
	        URL url = new URL("http://localhost:8080/iatciBroker/RequestSession");
	        URLConnection yc = url.openConnection();
	        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) { 
	            System.out.println(inputLine);
	            if (inputLine.startsWith("sessionID")) {
	            	String[] ps = inputLine.split("=");
	            	this.iatcisessionID = ps[1];
	            	System.out.println("requested sessionID = "+this.iatcisessionID);
	            }
	        }
	        in.close();
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			System.out.println("Requesting broker to start session: ");
	        URL url = new URL("http://localhost:8080/iatciBroker/RequestSessionStart?sessionID="+this.iatcisessionID);
	        URLConnection yc = url.openConnection();
	        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) { 
	            System.out.println("Reponse from Broker: "+inputLine);
	        }
	        in.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath()+"/sessions/session.jsp?sessionID="+iatcisessionID);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
