package com.detrics.iatci.IOS;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;

import javax.sql.RowSet;

import com.detrics.iatci.Message;
import com.detrics.iatci.Session;
import com.detrics.iatci.orm.*;

/**
 * Helper-class for Session, subclass that is IOS-specific.
 * Actually API for IOS.
 * 
 * @author martijn
 *
 */
public class IOS_Session extends Session {

	private static SimpleDateFormat fdq = new SimpleDateFormat("yyMMddHHmm");
	private static TimeZone utctimezone = TimeZone.getTimeZone("UTC");
	
	/**
	 * Creates a session, with an IOSmeredon flightparticipation as input.
	 * 
	 * @param fpID
	 */
	public String createSession(int resID) throws Exception {
		Collection<Participation> pax = new Vector<Participation>();
		Flight inb = new Flight();
		Flight outb = new Flight();
		IATA_Entity sender = null;
		IATA_Entity recipient = new IATA_Entity("","HUB","","");
		IATA_Entity dest1 = null;
		IATA_Entity dest2 = null;
		
		String paxoutb = "";
		int flightID = 0;
		
		RowSet rs = this.getQuery("SELECT * FROM flightparticipation LEFT JOIN person ON p_person=personID WHERE p_reservation="+resID);
		while (rs.next()) {
			flightID = rs.getInt("p_flight");
			Participation p = new Participation();
			p.setPaxname(rs.getString("pname"));
			p.setPaxfirstname(rs.getString("firstname"));
			p.setGender(rs.getString("gender"));
			RowSet conn = this.getQuery("SELECT * FROM recloc WHERE p_participation="+rs.getInt("fpID")+" AND isinbound=0 AND rlflightno>0");
			if (conn.next()) {
				IATA_Entity conndest1 = new IATA_Entity("", "", conn.getString("rlobp"), "");
				IATA_Entity conndest2 = new IATA_Entity("", "", conn.getString("rloap"), "");
				String outb_etd = getDateTime(conn.getInt("rloday"), conn.getString("rlotime"));;
				outb = new Flight().createFlight(conn.getString("airliner"), String.valueOf(conn.getString("rlflightno")), conndest1, conndest2, "", outb_etd);
				p.setPaxIDqueryingSystem(rs.getString("fpID"));
				p.setSeatclass("V");
				p.setTKNE("1234589001");
				p.setRecloc(conn.getString("recloc"));
			}
			
			pax.add(p);
		}
		
		RowSet f = this.getQuery("SELECT * FROM flight LEFT JOIN plan ON p_plan=planID LEFT JOIN line ON p_line=lineID LEFT JOIN airliner ON p_airliner=airlinerID WHERE flightID="+flightID);
		if (f.next()) {
			RowSet station1 = this.getQuery("SELECT * FROM destination WHERE destID="+f.getInt("dest1"));
			RowSet station2 = this.getQuery("SELECT * FROM destination WHERE destID="+f.getInt("dest2"));
			if (station1.next()) {
				sender = new IATA_Entity(f.getString("iatacode"), f.getString("icaocode"), station1.getString("destination"), station1.getString("cc"));
			}
			if (station2.next()) {
				dest1 = new IATA_Entity(f.getString("iatacode"), f.getString("icaocode"), station2.getString("destination"), station2.getString("cc"));				
			}
			inb.setFlightno(String.valueOf(f.getInt("cfnr")));
			inb.setAirliner(f.getString("iatacode"));
			long etd = f.getLong("utcscheduledt1");
			long eta = f.getLong("utcscheduledt2");
			inb.setETA(getDateTime(eta));
			inb.setETD(getDateTime(etd));
			
			inb.setDept(sender);
			inb.setArr(dest1);
		}
		
		String sessionID = createSession(Message.DCQCKI, pax, sender, recipient, inb, outb);
		this.storeResID(sessionID, resID);
		return sessionID;
	}

	private void storeResID(String sID, int resID) throws Exception {
		this.insert("UPDATE iatcisession SET iosresID="+resID+" WHERE sessionID='"+sID+"'");
	}
	
	private String getDateTime(long t) throws Exception {
		Date d2 = new Date(t);
		DateFormat fdqtime = new SimpleDateFormat("yyMMddHHmm");
		fdqtime.setTimeZone(utctimezone);
		return fdqtime.format(d2);
	}
	
	private String getDateTime(int dayofyear, String rlotime) throws Exception {
		SimpleDateFormat t = new SimpleDateFormat("yyyy D:HHmm");
		Date dy = new Date(System.currentTimeMillis());
		SimpleDateFormat sy = new SimpleDateFormat("yyyy");
		SimpleDateFormat sd = new SimpleDateFormat("D");
		String syear = sy.format(dy);
		String sday = sd.format(dy);
		int year = Integer.parseInt(syear);
		System.out.println(year);
		int doy =Integer.parseInt(sday);
		System.out.println(doy);
		System.out.println(rlotime);
		if (dayofyear<doy) year++;
		t.setTimeZone(utctimezone);
		java.util.Date d = t.parse(String.valueOf(year)+" "+String.valueOf(dayofyear)+":"+rlotime);
		long s = d.getTime();
		Date d2 = new Date(s);
		DateFormat fdqtime = new SimpleDateFormat("yyMMddHHmm");
		fdqtime.setTimeZone(utctimezone);
		return fdqtime.format(d2);
	}
	
}
