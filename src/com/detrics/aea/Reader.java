package com.detrics.aea;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Class for aviation reader using AEA protocol connected by ethernet
 * 
 * This class allows you to be able to:
 * 	-Read barcodes from tickets
 * 	-Open/Close shutter
 *  -Display different information about the printer
 * 	-AEA related functions for readers
 * 
 * See AEA TSpecs 2012 document
 * 
 * @author natacha
 *
 */
public class Reader {
	public static final byte STX = (byte)0x02;
	public static final byte ETX = (byte)0x03;
	private String _lastMessageFromReader = "";
	private String _lastRequestToReader = "";
	private boolean _successfulAction = false;
	private String _actionFeedback = "";
	private boolean _readerInit = false;
	private String _readerIP = "127.0.0.0";
	private int _portID = 0; 
	
	/**
	 * 
	 * Creates a connection with the reader.
	 * 
	 * @param printerIP the IP address of the reader
	 * @param portID the port used by the reader
	 * @throws Exception if a status request can not be sent to the reader, it means the reader can not be reached and this returns an Exception.
	 */
	Reader(String readerIP, int portID) throws Exception{
		_readerInit = false;
		_readerIP = readerIP;
		_portID = portID;
		if (_portID==0) _portID = 9100;
		if(askReader("T2#004000#"))
		{
			System.out.println("Reader init with : IP="+_readerIP+" on port="+_portID);
			_readerInit = true;
		}
		else
		{
			throw new Exception("Reader initialisation failed. Unabled to send a request to the reader.");
		}

	}
	

	
	public String scanCoupon()
	{
		String name = "ERROR";
		synchronized(this) {
			try {
				Socket socket = new Socket(_readerIP, _portID);
				OutputStream outputStream = socket.getOutputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				//For convenience a ArrayList was used to encode the message to be sent to the printer.
				//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.

				//Receiving output from Printer

				byte readerOutput = 1;
				String fromReader = "";
				while((readerOutput = (byte)socket.getInputStream().read()) != 3)
				{
					if(readerOutput != 2) //Avoid the inclusion of STX in the user output
						fromReader += (char)readerOutput;

				}
				System.out.println(fromReader);
				name = fromReader.substring(fromReader.indexOf("#")).substring(4);
				outputStream.flush();
				outputStream.close();
				in.close();
				socket.close();
			} catch(Exception e) {
				System.out.println("ERROR@"+_readerIP+" : "+e.getMessage());
			}
			return name;
	}
	}

	public void scan()
	{

		scanCoupon();
	}
	
	//Converts Byte[] to byte[]. Used to send data to the printer as it can only accept byte[].
	byte[] toPrimitives(Byte[] oBytes)
	{

		byte[] bytes = new byte[oBytes.length];
		for(int i = 0; i < oBytes.length; i++){
			bytes[i] = oBytes[i];
		}
		return bytes;
	}
	
	//The general print function that takes the formated AEA command and send it to the printer.
	private boolean sendToReader(byte[] AEAcommand)
	{
		synchronized(this) {
			try {
				Socket socket = new Socket(_readerIP, _portID);
				OutputStream outputStream = socket.getOutputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				//For convenience a ArrayList was used to encode the message to be sent to the printer.
				//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.
				outputStream.write(AEAcommand,0,AEAcommand.length);

				//Receiving output from Printer
				byte printerOutput = 1;
				String fromPrinter = "";
				while((printerOutput = (byte)socket.getInputStream().read()) != 3)
				{
					if(printerOutput != 2) //Avoid the inclusion of STX in the user output
						fromPrinter += (char)printerOutput;

				}
				System.out.println(fromPrinter);
				outputStream.flush();
				outputStream.close();
				in.close();
				socket.close();
				return true;
			} catch(Exception e) {
				System.out.println("ERROR@"+_readerIP+" : "+e.getMessage());
			}
		}
		return false;
	}
	
	//Convert the string to bytes and add the STX and ETX bytes.
	private byte[] stringToByteArray(String command)
	{
		byte[] commandReadyForPrinter = null;
		ArrayList<Byte> AEACommand = new ArrayList<Byte>();
		AEACommand.add(STX);
		byte[] sToByte = command.getBytes();
		for(int i = 0; i < sToByte.length;i++)
		{
			AEACommand.add(sToByte[i]);
		}

		AEACommand.add(ETX);
		//For convenience a ArrayList was used to encode the message to be sent to the printer.
		//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.
		Byte[] buffer = (Byte[]) AEACommand.toArray(new Byte[0]);
		commandReadyForPrinter = toPrimitives(buffer);
		return commandReadyForPrinter;
	}
	//This function is used for any AEA command that only ask for information without printing.
	public boolean askReader(String request){
		boolean succesful = false;
		String statutMessage = "Ask status : ";
		//_lastCommandSent = request;
		if(sendToReader(stringToByteArray(request)))
		{
			statutMessage+="Sent.";
			succesful = true;
		}
		else
		{
			statutMessage+="Failed.";
			succesful = false;
		}
		//System.out.println(statutMessage);
		_actionFeedback = statutMessage;
		_successfulAction = succesful;
		return succesful;
	}
	
	//command for closing the front insertion slot (shutter)
	public boolean closeShutter()
	{
		String command = "CW";
		boolean successful = false;
		String statutMessage = "Closing shutter : ";
		if(sendToReader(stringToByteArray(command)))
		{
			if(_lastMessageFromReader.contains("CWOK"))
			{
				statutMessage+="successful.";
				successful = true;
			}
			else
			{
				statutMessage+="Failed : ";
				if(_lastMessageFromReader.contains("ERR"))
				{
					int i = _lastMessageFromReader.indexOf("ERR");
					switch(_lastMessageFromReader.charAt(i+3))
					{
					case '2': 
						statutMessage+="ERR2. Illogical command : "+_lastMessageFromReader.substring(i+4)+" (A down line loading (PECTAB, LOGO, etc) is in progress).";
						break;
					case '7':
						statutMessage+="ERR7. A check-in/revalidation is in progress.";
						break;
					}
				}
				else
					statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromReader+"";
			}
		}
		else
		{
			successful = false;
			statutMessage+="Unable to reach printer.";
		}	
		_actionFeedback = statutMessage;
		_successfulAction = successful;
		return successful;
	}
	
	//command for opening the front insertion slot (shutter)
	public boolean openShutter()
	{
		String command = "CR";
		boolean successful = false;
		String statutMessage = "Opening shutter : ";
		if(sendToReader(stringToByteArray(command)))
		{
			if(_lastMessageFromReader.contains("CROK"))
			{
				statutMessage+="successful.";
				successful = true;
			}
			else
			{
				statutMessage+="Failed : ";
				if(_lastMessageFromReader.contains("ERR"))
				{
					int i = _lastMessageFromReader.indexOf("ERR");
					switch(_lastMessageFromReader.charAt(i+3))
					{
					case '2': 
						statutMessage+="ERR2. Illogical command : "+_lastMessageFromReader.substring(i+4)+" (A down line loading (PECTAB, LOGO, etc) is in progress).";
						break;
					case '7':
						statutMessage+="ERR7. A check-in/revalidation is in progress.";
						break;
					}
				}
				else
					statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromReader+"";
				successful = false;
			}
		}
		else
		{
			successful = false;
			statutMessage+="Unable to reach printer.";
		}	
		_actionFeedback = statutMessage;
		_successfulAction = successful;
		return successful;
	}
}
