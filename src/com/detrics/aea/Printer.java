package com.detrics.aea;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


/**
 * Class for aviation printer using AEA protocol connected by ethernet
 * 
 * This class allows you to be able to:
 * 	-Print a boarding pass
 * 	-Print a bagtag
 *  -Display different information about the printer
 * 	-AEA related function such as loading/cancelling PECTABS (AEA TSpecs 2012 p3)
 * 
 * See AEA TSpecs 2012 document
 * 
 * @author natacha
 *
 */

public class Printer {
	public static final byte STX = (byte)0x02; //Start of command byte
	public static final byte ETX = (byte)0x03; //End of command byte
	
	//Connectivity 
	private String _printerIP = ""; //The IP address of the printer
	private int _portID = 0; //The port used by the printer
	
	//Feedback from printer
	private String _lastMessageFromPrinter = ""; //Stores the last message sent by the printer 
	private String _lastCommandSent = ""; //Saves the last command that was sent
	private String _actionFeedback = ""; //String that indicates how went the last action (command sent/ failed and error message for example)
	private boolean _successfulAction = false; //Boolean true of last action was successfully sent to the printer and returned without an error
	
	//General information
	private boolean _SCNR = false; //Boolean true if Stock control number is on
	private PrinterMode _printerMode; //The printer can be in 3 different modes: CheckIn, Ticketing, None
	private String _separator = "#"; //The separator used in all AEA commands.
	private String _programVersion; //In AEA the printer can store a program in its memory. This gives you the version of it.
	private String _transactionCode; //Each printer possesses a hard coded transaction code from 3 to 5 letters. Every printer response starts with this code.
	
	//More information about the printer
	private boolean _PECTABinMemory = false; //Boolean true if any PECTAB was found in the memory of the printer
	private boolean _logosInMemory = false; //Boolean true if any logo was found in the memory of the printer
	private boolean _templatesInMemory = false; //Boolean true if any template was found in the memory of the printer
	private boolean _fontsInMemory = false; //Boolean true if any font was found in the memory of the printer
	private List<String> _PECTABLoaded; //Contains all the addresses of the PECTAB stored in the memory of the printer (example: V1)
	private List<String> _logosLoaded; //Contains all the addresses of the logos stored in the memory of the printer (example: V1)
	private List<String> _templatesLoaded; //Contains all the addresses of the templates stored in the memory of the printer (example: V1)
	private List<String> _fontsLoaded; //Contains all the addresses of the fonts stored in the memory of the printer (example: V1)
	private List<String> _binParameters; //For each bin, AEA sends info about the bin as follow : bcvs (more info AEA TSpecs 2012 p42)
	private String _defaultBin; //Tells which bin is the default bin
	private String _frontFeedParameter; //Front feed parameter: FV= verify on, FN = verify off
	private String _binStatusRequestParameter; //
	
	private boolean _printerInit = false; //Boolean true if printer was correctly iniatialized
	
	//Following attributes has yet to be fully implemented:
	private PrinterEmulation _printerEmulation = PrinterEmulation.ATB; //Can be ATB, BTP or service emulation. ATB = boarding pass, BTP = bagtag, service = prints exactly what is sent
	
	
	
	/**
	 * 
	 * Creates a connection with the printer and request information about the printer.
	 * 
	 * @param printerIP the IP address of the printer
	 * @param portID the port used by the printer
	 * @throws Exception if a status request can not be sent to the printer, it means the printer can not be reached and this returns an Exception.
	 */
	Printer(String printerIP, int portID) throws Exception{
		_PECTABLoaded = new ArrayList<String>();
		_logosLoaded = new ArrayList<String>();
		_templatesLoaded = new ArrayList<String>();
		_fontsLoaded = new ArrayList<String>();
		_binParameters = new ArrayList<String>();
		
		_printerInit = false;
		_printerIP = printerIP;
		_portID = portID;
		if (_portID==0) _portID = 9100;
		System.out.println("Printer init with : IP="+_printerIP+" on port="+_portID);
		//if(updatePectabAndPrinterModeInformations() && requestPrinterGeneralStatus())
			_printerInit = true;
		//else
			//throw new Exception("Printer intialization failed.");

	}
	
	/**
	 * Return the last command sent to the printer.
	 * @return String last command sent to the printer
	 */
	public String getLastCommandSent()
	{
		return _lastCommandSent;
	}
	
	/**
	 * Return the exact message last sent by the printer
	 * @return String last message from printer
	 */
	public String getPrinterOutput()
	{
		return _lastMessageFromPrinter;
	}
	
	/**
	 * Get the result of last action (in human understandable text). Indicates reason for error or that command was successful
	 * @return String action feedback
	 */
	public String getFeedback()
	{
		return _actionFeedback;
	}
	
	/**
	 * Indicates if the last action tried was successful or not
	 * @return boolean true if last action was successful 
	 */
	public boolean successfulAction()
	{
		return _successfulAction;
	}
	
	/**
	 * Return the current printer emulation (ATB, BTP, service)
	 * ATB is used for printing boarding pass
	 * BTP is used for printing bagtag
	 * service prints exactly what is sent to the printer
	 * 
	 * This has yet to be implemented
	 * @return PrinterEmulation current printer emulation
	 */
	public PrinterEmulation getPrinterEmulationSetup()
	{
		return _printerEmulation;
	}
	
	/**
	 * Return the current printer mode (Checkin, ticketing, none)
	 * Printer mode none is for devices without mode change example : Gatereader
	 * @return PrinterMode current printer mode
	 */
	public PrinterMode getPrinterMode()
	{
		return _printerMode;
	}
	
	/**
	 * Break down the SCNR (stock control number) info that get sent by the printer
	 * @param SCNRinfo
	 * @return String with every information about SCNR
	 */
	private String getSCNRinfo(String SCNRinfo)
	{
		String info = "";
		int numberOfCoupons = 0;
		String[] coupons = {""};
		if(SCNRinfo.contains(_separator))
		{
			coupons = SCNRinfo.split(_separator);
		}
		else
		{
			coupons[0] = SCNRinfo;
		}
		
		for(int k = 0; k < coupons.length;k++)
		{
			numberOfCoupons = 0;
			for(int i = coupons[k].length()-1; i >= 0;i--)
			{
				if(coupons[k].charAt(i) > 'A' && coupons[k].charAt(i) < 'Z')
					numberOfCoupons++;
				else
					break;
			}
			info+="  Stockcontrol number of first coupon : ";
			if(coupons[k].startsWith("V"))
				info+=coupons[k].substring(1,coupons[k].length()-numberOfCoupons)+" - voided";
			else
				info+=coupons[k].substring(0,coupons[k].length()-numberOfCoupons);
			info+="\n";
			int j=2;
			for(int i = (coupons[k].length()-numberOfCoupons); i < coupons[k].length(); i++)
			{
				info+="    Coupon "+j+" : ";
				switch(coupons[k].charAt(i))
				{
					case 'C':
						info+="Flight coupon successfuly printed.";
						break;
					case 'R':
						info+="Receipt coupon successfuly printed.";
						break;
					case 'U':
						info+="Auditor coupon successfuly printed.";
						break;
					case 'I':
						info+="Issuing coupon successfuly printed.";
						break;
					case 'F':
						info+="Credit Charge Form coupon successfuly printed.";
						break;
					case 'X':
						info+="Free Form coupon successfuly printed.";
						break;
					case 'V':
						info+="Coupon voided.";
						break;
				}
				info+="\n";
				j++;
			}
		}
		
		return info;
	}
	
	/**
	 * Update the following information about the printer
	 *  -format codes and version numbers of PECTAB(s) residing in ATBPR memory
	 *  -printer operational mode (ticketing, checkin, none)
	 * Uses AEA command PS. More about it in AEA TSpecs 2012 p36
	 * @return true if sending the status request was possible
	 */
	public boolean updatePectabAndPrinterModeInformations()
	{
		String command = "PS";
		String statutMessage="Printer status request : ";
		boolean successful = false;
		if(sendToPrinter(stringToByteArray(command)))
		{
			if(_lastMessageFromPrinter.contains("PSOK"))
			{
				statutMessage+="Successful.\n";
				statutMessage+="Printer is without mode change capability.\n";
				if(_lastMessageFromPrinter.length() > 4)
				{
					int i = _lastMessageFromPrinter.indexOf("PSOK")+4;
					String[] infos = _lastMessageFromPrinter.substring(i).split(_separator);
					if(!_lastMessageFromPrinter.substring(i).startsWith(_separator))
					{
						_PECTABLoaded.clear();
						int pectabCount = infos[0].length()/2;
						for(int j = 0; j < pectabCount ; j++)
						{
							_PECTABLoaded.add(infos[0].substring((j*2), (j*2)+2));
						}
						_PECTABinMemory = true;
						statutMessage+= "PECTABs in memory : ";
						for(String pectab : _PECTABLoaded)
						{
							statutMessage+=pectab+" ";
						}
						statutMessage+="\n";
					}
					else
					{
						statutMessage+="No PECTABs found in memory\n";
						_PECTABinMemory = false;
					}
					if(infos.length > 1)
						statutMessage+="Status info acc. PECTAB element steering command \"keep for confirmation and status\" : \n";
					for(int k = 1; k < infos.length;k++)
					{
						statutMessage+= "  Status info field number "+infos[k].substring(0,2)+" : "+infos[k].substring(2)+ "\n";
					}
				}
				_printerMode = PrinterMode.none;
				successful = true;
			}
			else if(_lastMessageFromPrinter.contains("TSOK"))
			{
				statutMessage+="Successful.\n";
				statutMessage+="Printer is in Ticketing/Revalidation mode.\n";
				if(_lastMessageFromPrinter.length() > 4)
				{
					int i = _lastMessageFromPrinter.indexOf("TSOK")+4;
					String[] infos;
					if(_lastMessageFromPrinter.contains("SC"))
					{
						String[] s = _lastMessageFromPrinter.substring(i).split("SC");
						statutMessage+="SCNR info: \n";
						try {
							statutMessage+=getSCNRinfo(s[1]);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						infos = s[0].split(_separator);
					}
					else
					{
						infos = _lastMessageFromPrinter.substring(i).split(_separator);
					}
					if(!_lastMessageFromPrinter.substring(i).startsWith(_separator))
					{
	
						_PECTABLoaded.clear();
						int pectabCount = infos[0].length()/2;
						for(int j = 0; j < pectabCount ; j++)
						{
							_PECTABLoaded.add(infos[0].substring((j*2), (j*2)+2));
						}
						_PECTABinMemory = true;
						statutMessage+= "PECTABs in memory : ";
						for(String pectab : _PECTABLoaded)
						{
							statutMessage+=pectab+" ";
						}
						statutMessage+="\n";
					}
					else
					{
						statutMessage+="No PECTABs found in memory\n";
						_PECTABinMemory = false;
					}
					if(infos.length > 1)
						statutMessage+="Status info acc. PECTAB element steering command \"keep for confirmation and status\" : \n";
					for(int k = 1; k < infos.length;k++)
					{
						statutMessage+= "  Status info field number "+infos[k].substring(0,2)+" : "+infos[k].substring(2)+ "\n";
					}
				}
				_printerMode = PrinterMode.ticketing;
				successful = true;
			}
			else if(_lastMessageFromPrinter.contains("CSOK"))
			{
				statutMessage+="Successful.\n";
				statutMessage+="Printer is in Check-in mode.\n";
				if(_lastMessageFromPrinter.length() > 4)
				{
					int i = _lastMessageFromPrinter.indexOf("CSOK")+4;
					String[] infos = _lastMessageFromPrinter.substring(i).split(_separator);
					if(!_lastMessageFromPrinter.substring(i).startsWith(_separator))
					{
						_PECTABLoaded.clear();
						int pectabCount = infos[0].length()/2;
						for(int j = 0; j < pectabCount ; j++)
						{
							_PECTABLoaded.add(infos[0].substring((j*2), (j*2)+2));
						}
						_PECTABinMemory = true;
						statutMessage+= "PECTABs in memory : ";
						for(String pectab : _PECTABLoaded)
						{
							statutMessage+=pectab+" ";
						}
						statutMessage+="\n";
					}
					else
					{
						statutMessage+="No PECTABs found in memory\n";
						_PECTABinMemory = false;
					}
					if(infos.length > 1)
						statutMessage+="Status info acc. PECTAB element steering command \"keep for confirmation and status\" : \n";
					for(int k = 1; k < infos.length;k++)
					{
						statutMessage+= "  Status info field number "+infos[k].substring(0,2)+" : "+infos[k].substring(2)+ "\n";
					}
				}
				_printerMode = PrinterMode.checkin;
				successful = true;
			}
			else
			{
				if(_lastMessageFromPrinter.contains("ERR"))
				{
					int i = _lastMessageFromPrinter.indexOf("ERR");
					switch(_lastMessageFromPrinter.charAt(i+3))
					{
					case '2': 
						statutMessage+="ERR2. Illogical command : "+_lastMessageFromPrinter.substring(i+4);
						break;
					case '7':
						statutMessage+="ERR7. A check-in/revalidation is in progress.";
						break;
						
					}
				}
				else
					statutMessage += "Unknown error. Output from printer : "+_lastMessageFromPrinter;
				successful = false;
			}
		}
		_actionFeedback = statutMessage;
		_successfulAction = successful;
		return successful;
	}
	
	
	/**
	 * Gets the general information about the printer
	 * 	-Addresses of the PECTABs loaded in the memory
	 *  -Addresses of the logos loaded in the memory
	 *  -Version of the program loaded in the memory
	 *  -Addresses of the templates loaded in the memory
	 *  -Addresses of the fonts loaded in the memory
	 *  -Hardcoded transaction code of the printer
	 *  -The bin setup
	 *  
	 *  Uses the AEA command US. More information about it in AEA TSpecs 2012 p46
	 * @return true if the status request was correctly sent to the printer
	 */
	public boolean requestPrinterGeneralStatus()
	{
		if(askPrinter("US"))
		{
			String[] splitMessage;
			//Check for PECTAB loaded in the printer
			int i = _lastMessageFromPrinter.indexOf("PSOK");
			if(!_lastMessageFromPrinter.startsWith("LSOK", i+4)) //This means after PSOK are listed the PECTABs in memory
			{
				splitMessage = _lastMessageFromPrinter.substring(i+4).split("LSOK");
				int PECTABCount = splitMessage[0].length()/2;
				for(int j =0; j < PECTABCount; j++)
				{
					_PECTABLoaded.add(_lastMessageFromPrinter.substring((i+4)+(j*2),(i+4)+(j*2)+2));
				}
				_PECTABinMemory = true;
			}
			else
			{
				_PECTABinMemory = false;
				_PECTABLoaded.clear();
			}
			
			//Check for logos loaded in the printer
			i = _lastMessageFromPrinter.indexOf("LSOK");
			if(!_lastMessageFromPrinter.startsWith("PVOK", i+4)) //This means after LSOK are listed the logos in memory
			{
				splitMessage = _lastMessageFromPrinter.substring(i+4).split("PVOK");
				int LogosCount = splitMessage[0].length()/2;
				for(int j =0; j < LogosCount; j++)
				{
					_logosLoaded.add(_lastMessageFromPrinter.substring((i+4)+(j*2),(i+4)+(j*2)+2));
				}
				_logosInMemory = true;
			}
			else
			{
				_logosInMemory = false;
				_logosLoaded.clear();
			}
			
			//Get the program version
			i = _lastMessageFromPrinter.indexOf("PVOK");
			splitMessage = _lastMessageFromPrinter.substring(i+4).split("TAOK");
			_programVersion = splitMessage[0];
			
			//Check for templates loaded in the printer
			i = _lastMessageFromPrinter.indexOf("TAOK");
			if(!_lastMessageFromPrinter.startsWith("FSOK", i+4)) //This means after TAOK are listed the templates in memory
			{
				splitMessage = _lastMessageFromPrinter.substring(i+4).split("FSOK");
				int templatesCount = splitMessage[0].length()/2;
				for(int j =0; j < templatesCount; j++)
				{
					_templatesLoaded.add(_lastMessageFromPrinter.substring((i+4)+(j*2),(i+4)+(j*2)+2));
				}
				_templatesInMemory = true;
			}
			else
			{
				_templatesInMemory = false;
				_templatesLoaded.clear();
			}
			
			//Check for fonts loaded in the printer
			i = _lastMessageFromPrinter.indexOf("FSOK");
			if(!_lastMessageFromPrinter.startsWith("CTOK", i+4)) //This means after TAOK are listed the templates in memory
			{
				splitMessage = _lastMessageFromPrinter.substring(i+4).split("CTOK");
				int fontsCount = splitMessage[0].length()/2;
				for(int j =0; j < fontsCount; j++)
				{
					_fontsLoaded.add(_lastMessageFromPrinter.substring((i+4)+(j*2),(i+4)+(j*2)+2));
				}
				_fontsInMemory = true;
			}
			else
			{
				_fontsInMemory = false;
				_fontsLoaded.clear();
			}
			
			
			//Get the transaction code
			i = _lastMessageFromPrinter.indexOf("CTOK");
			splitMessage = _lastMessageFromPrinter.substring(i+4).split("BTOK");
			_transactionCode = splitMessage[0];
			
			//Get the bin setup
			i = _lastMessageFromPrinter.indexOf("BTOK");
			int beginningOfBinParameters = i+4;
			for(int j = 0; j < 3; j++) //get individual info about all 3 bins
			{
				int beginningOfCurrentBinParameters = beginningOfBinParameters+(j*4);
				int endOfCurrentBinParameters = beginningOfCurrentBinParameters+4;
				_binParameters.add(_lastMessageFromPrinter.substring(beginningOfCurrentBinParameters,endOfCurrentBinParameters));
				i = beginningOfBinParameters + j*4;
			}
			_defaultBin = _lastMessageFromPrinter.substring(i,i+2);
			i+=2;
			_frontFeedParameter = _lastMessageFromPrinter.substring(i,i+2);
			i+=2;
			_binStatusRequestParameter = _lastMessageFromPrinter.substring(i,i+2);
			

			_successfulAction = true;
			_actionFeedback = "Successfully received printer status information from printer.";
			return true;
		}
		else
		{
			_actionFeedback = "Error! Printer status information from printer could not be received.";
			_successfulAction = false;
			return false;
		}
	}
	
	
	/**
	 * Load the given PECTAB in the memory of the printer to be used later to print boarding pass (ATB mode) or bagtag (BTP mode)
	 * 
	 * More information about PECTAB and PECTAB loading in AEA TSpecs 2012 p32
	 * @param PECTAB to be loaded in the memory
	 * @return true if the load function was successful
	 */
	public boolean loadPECTAB(String PECTAB)
	{
		boolean succesful = false;
		String statutMessage = "PECTAB loading status : ";
		_lastCommandSent = PECTAB;
		if(sendToPrinter(stringToByteArray(PECTAB)))
		{
			statutMessage+="Loaded with the following format code and version : "+_lastCommandSent.substring(5, 7)+ ".";
			if(_lastMessageFromPrinter.contains("PTOK"))
			{
				int i = _lastMessageFromPrinter.indexOf("PTOK")+4;
				int PECTABCount = (_lastMessageFromPrinter.substring(i).length()/2);
				_PECTABinMemory = true;
				_PECTABLoaded.clear();
				for(int j = 0; j < PECTABCount;j++)
				{
					_PECTABLoaded.add(_lastMessageFromPrinter.substring(i+(j*2), i+(j*2)+2));
				}
				succesful = true;
			}
			else
			{
				statutMessage+="Failed : ";
				if(_lastMessageFromPrinter.contains("ERR"))
				{
					int i = _lastMessageFromPrinter.indexOf("ERR");
					switch(_lastMessageFromPrinter.charAt(i+3))
					{
					case '2': 
						statutMessage+="ERR2. Illogical command : "+_lastMessageFromPrinter.substring(i+4);
						break;
					case '7':
						statutMessage+="ERR7. A check-in/revalidation is in progress.";
						break;
					case '8':
						statutMessage+="ERR8. Illogical data ";
						if(_lastMessageFromPrinter.substring(i+4) == "00")
						{
							statutMessage+="in PECTAB header.";
						}
						else
						{
							statutMessage+="at element "+_lastMessageFromPrinter.substring(i+4);
						}
						break;
					case '9':
						statutMessage+="ERR9. The PECTAB exceeds the memory capacity of the ATBR.";
						break;	
					}
				}
				else
					statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromPrinter+"";
				succesful = false;
			}
		}
		else
		{
			succesful = false;
			statutMessage+="Unable to reach printer.";
		}
			
			
		
		
		_actionFeedback = statutMessage;
		_successfulAction = succesful;
		return succesful;
	}
	
	/**
	 * Remove a PECTAB from the memory of the printer. 
	 * 
	 * Uses the AEA command PC. More information about it in AEA TSpecs 2012 p35
	 * @param pectabToCancel Address of the PECTAB to cancel (ex V1)
	 * @return true PECTAB was correctly removed from the memory of the printer
	 */
	public boolean cancelPECTAB(String pectabToCancel)
	{
		boolean succesful = false;
		List<String> allPectabs = new ArrayList<String>();
		String cancelCommand = "PC";
		String statutMessage = "";
		int numberOfPectab = (pectabToCancel.length()/2);
		for(int i = 0; i < numberOfPectab; i++)
		{
			allPectabs.add(pectabToCancel.substring((i*2),(i*2)+2));
		}
		if(_PECTABLoaded.containsAll(allPectabs))
		{
			statutMessage = "PECTAB cancel status : ";
			if(pectabToCancel != "")
				cancelCommand+=pectabToCancel;
			_lastCommandSent = cancelCommand;
			if(sendToPrinter(stringToByteArray(cancelCommand)))
			{
				if(_lastMessageFromPrinter.contains("PCOK"))
				{
					if(allPectabs.isEmpty())
						statutMessage+="Cancelled all PECTABs in memory: ";
					else
						statutMessage+="Cancelled the following PECTABs : ";
					for(String pectab : allPectabs)
					{
						statutMessage+=(pectab+" ");
					}
					_PECTABLoaded.removeAll(allPectabs);
					if(_PECTABLoaded.isEmpty())
						_PECTABinMemory = false;
					else
						_PECTABinMemory = true;
				}
				succesful = true;
			}
			else
			{
				statutMessage+="Failed : ";
				statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromPrinter+"";
				succesful = false;
			}
		}
		else
		{
			statutMessage = "Unable to cancel PECTAB "+pectabToCancel+" : not in the memory of the printer.";
			_successfulAction = false;
		}
		_actionFeedback = statutMessage;
		_successfulAction = succesful;
		return succesful;
	}
	
	/**
	 * Converts Byte[] to byte[]. Used to send data to the printer as it can only accept byte[].
	 * @param oBytes Byte[] data
	 * @return the Byte[] data as byte[]
	 */
	byte[] toPrimitives(Byte[] oBytes)
	{

		byte[] bytes = new byte[oBytes.length];
		for(int i = 0; i < oBytes.length; i++){
			bytes[i] = oBytes[i];
		}
		return bytes;
	}
	
	/**
	 * Takes the formated AEA command (in byte[]) and send it to the printer after opening a socket.
	 * Also gets the answer from the printer and stores it inside _lastMessageFromPrinter.
	 * 
	 * If the printer is unable to send back an answer (abnormal behaviour), this function get stuck in a loop.
	 * 
	 * @param AEAcommand in byte[]
	 * @return true if the command was succesfully sent and a response was given by the printer.
	 */
	private boolean sendToPrinter(byte[] AEAcommand)
	{
		synchronized(this) {
			try {
				Socket socket = new Socket(_printerIP, _portID);
				OutputStream outputStream = socket.getOutputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				//For convenience a ArrayList was used to encode the message to be sent to the printer.
				//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.
				outputStream.write(AEAcommand,0,AEAcommand.length);

				//Receiving output from Printer
				byte printerOutput = 1;
				String fromPrinter = "";
				while((printerOutput = (byte)socket.getInputStream().read()) != 3)
				{
					if(printerOutput != 2) //Avoid the inclusion of STX in the user output
						fromPrinter += (char)printerOutput;

				}
				_lastMessageFromPrinter = fromPrinter;
				outputStream.flush();
				outputStream.close();
				in.close();
				socket.close();
				return true;
			} catch(Exception e) {
				System.out.println("ERROR@"+_printerIP+" : "+e.getMessage());
			}
		}
		return false;

	}
	
	/**
	 * Print a boarding pass p18
	 * @param boardingPass - the boarding pass command as specified in AEA TSpecs 2012 p18
	 * @return true if boarding pass was printed
	 */
	public boolean printBoardingPass(String boardingPass) {
		boolean successful = false;
		String statutMessage = "Boarding pass printing status : ";
		if(sendToPrinter(stringToByteArray(boardingPass)))
		{
			if(_lastMessageFromPrinter.contains("PROK"))
			{
				statutMessage+="successful.";
				successful = true;
			}
			else
			{
				statutMessage+="Failed - ";
				if(_lastMessageFromPrinter.contains("ERR"))
				{
					int i = _lastMessageFromPrinter.indexOf("ERR");
					switch(_lastMessageFromPrinter.charAt(i+3))
					{
						case '6': 
							statutMessage+="ERR6. PECTAB not available.";
							break;
						case '3':
							statutMessage+="ERR3 - PECTAB error at "+_lastMessageFromPrinter.substring(_lastMessageFromPrinter.indexOf("VSR#")+4);
							break;
						default:
							statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromPrinter+"";
							break;
					}
				}
				else
					statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromPrinter+"";
				successful = false;
			}
		}
		else
		{
			successful = false;
			statutMessage+="Unable to reach printer.";
		}	
		_actionFeedback = statutMessage;
		_successfulAction = successful;
		return successful;
	}
	
	/**
	 * THIS FUNCTION IS NOT FINISHED.
	 * 
	 * Print a barcode/bagtag. Enables to give an offset to move the bagtag in the vertical plane
	 * 
	 * Currently in test-mode. Pectab for this is hardcoded. Error handling is missing
	 * 
	 * @param barcode - Barcode command as specified in AEA TSpecs 2012 p 473
	 * @param offset - pixels element has to be moved verticaly positive is up
	 * @return true if bagtag was successfully printed
	 */
	public boolean printBarcode(String barcode, int offset)
	{
		boolean successful = false;
		String statutMessage = "Boarding pass printing status : ";
		String pectabbarcode ="BTT0101]F0510250=#01C0 5"+(offset+255 < 100 ? "0" : "")+(offset+255 < 10 ? "0" : "")+(255+offset)+"490201"
				+ "#02C0 5"+(offset+255 < 100 ? "0" : "")+(offset+255 < 10 ? "0" : "")+(255+offset)+"150201#"
				+ "03C0 5"+(offset+255 < 100 ? "0" : "")+(offset+255 < 10 ? "0" : "")+(255+offset)+"090201#"
				+ "04C0 5"+(offset+259 < 100 ? "0" : "")+(offset+259 < 10 ? "0" : "")+(259+offset)+"180201#"
				+ "05C0 1"+(offset < 100 ? "0" : "")+(offset < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "06K0 A"+(offset+3 < 100 ? "0" : "")+(offset+3 < 10 ? "0" : "")+(003+offset)+"250202#"
				+ "07C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "08C0 5"+(259+offset)+"490201#"
				+ "09I1 A"+(offset+7 < 100 ? "0" : "")+(offset+7 < 10 ? "0" : "")+(007+offset)+"250632#"
				+ "0AC0 5"+(474+offset)+"120201#"
				+ "0BC0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "10C0M1"+(216+offset)+"020303#"
				+ "11C0M1"+(216+offset)+"130303#"
				+ "12C0M1"+(216+offset)+"310303#"
				+ "15C0MA"+(224+offset)+"261006#"
				+ "16C0MA"+(220+offset)+"260302#"
				+ "17C0 1"+(offset < 100 ? "0" : "")+(offset < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "18C0MA"+(224+offset)+"440605#"
				+ "20C0M1"+(196+offset)+"020303#"
				+ "21C0M1"+(196+offset)+"130303#"
				+ "22C0M1"+(196+offset)+"310303#"
				+ "25C0MA"+(204+offset)+"261006#"
				+ "26C0MA"+(200+offset)+"260302#"
				+ "27C0 1"+(offset < 100 ? "0" : "")+(offset < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "28C0MA"+(204+offset)+"440605#"
				+ "30C0M1"+(176+offset)+"020303#"
				+ "31C0M1"+(176+offset)+"130303#"
				+ "32C0M1"+(176+offset)+"310303#"
				+ "35C0MA"+(184+offset)+"261006#"
				+ "36C0MA"+(180+offset)+"260302#"
				+ "37C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "38C0MA"+(184+offset)+"440605#"
				+ "40C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "41C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "42C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "45C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "46C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "47C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "48C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "77C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "78C0M2"+(159+offset)+"061408#"
				+ "80C0 1"+(offset+14 < 100 ? "0" : "")+(offset+14 < 10 ? "0" : "")+(014+offset)+"050201#"
				+ "81C0 1"+(offset+14 < 100 ? "0" : "")+(offset+14 < 10 ? "0" : "")+(014+offset)+"110201#"
				+ "82C0 1"+(offset+14 < 100 ? "0" : "")+(offset+14 < 10 ? "0" : "")+(014+offset)+"230201#"
				+ "85C0 1"+(offset+14 < 100 ? "0" : "")+(offset+14 < 10 ? "0" : "")+(014+offset)+"380201#"
				+ "86C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "87C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "88C0 1"+(offset+0 < 100 ? "0" : "")+(offset+0 < 10 ? "0" : "")+(000+offset)+"990201#"
				+ "A1C0 5"+(491+offset)+"490201=30#"
				+ "A2C0 5"+(491+offset)+"430201=31#"
				+ "A3C0 5"+(491+offset)+"300201=32#"
				+ "A4C0 5"+(491+offset)+"130201=35#"
				+ "A5C0 5"+(487+offset)+"490201=20#"
				+ "A6C0 5"+(487+offset)+"430201=21#"
				+ "A7C0 5"+(487+offset)+"300201=22#"
				+ "A8C0 5"+(487+offset)+"130201=25#"
				+ "A9C0 5"+(483+offset)+"490201=10#"
				+ "AAC0 5"+(483+offset)+"430201=11#"
				+ "ABC0 5"+(483+offset)+"300201=12#"
				+ "ACC0 5"+(483+offset)+"130201=15#"
				+ "ADC0 E"+(479+offset)+"250201=16#"
				+ "AEC0 5"+(474+offset)+"490201BAG:#"
				+ "AFC0 5"+(474+offset)+"390201=02#"
				+ "B0C0 5"+(474+offset)+"350201/#"
				+ "B1C0 5"+(474+offset)+"330201=03#"
				+ "B2C0 5"+(474+offset)+"240201B/NO:#"
				+ "B3C0 5"+(470+offset)+"490201=01#"
				+ "B4K0 E"+(465+offset)+"250303=06#"
				+ "B5I1 E"+(461+offset)+"260532=09#"
				+ "B6C0 5"+(259+offset)+"420201/#"
				+ "B7C0 5"+(259+offset)+"400201=82#"
				+ "B8C0 5"+(255+offset)+"110201/#"
				+ "B9S0MA"+(235+offset)+"250250#"
				+ "BAC0M1"+(231+offset)+"020302TO#"
				+ "BBS0MA"+(215+offset)+"250250#"
				+ "BCC0M1"+(211+offset)+"020302VIA#"
				+ "BDS0MA"+(195+offset)+"250250#"
				+ "BEC0M1"+(191+offset)+"020302VIA#"
				+ "BFS0MA"+(175+offset)+"250250#"
				+ "C0I1MC"+(132+offset)+"474341=09#"
				+ "C1I1MA"+(offset+69 < 100 ? "0" : "")+(offset+69 < 10 ? "0" : "")+(69+offset)+"254041=09#"
				+ "C2K0MA"+(offset+65 < 100 ? "0" : "")+(offset+65 < 10 ? "0" : "")+(065+offset)+"250202=06#"
				+ "C3C0 1"+(offset+54 < 100 ? "0" : "")+(offset+54 < 10 ? "0" : "")+(054+offset)+"380201=85#"
				+ "C4C0 1"+(offset+54 < 100 ? "0" : "")+(offset+54 < 10 ? "0" : "")+(054+offset)+"230201=82#"
				+ "C5C0 1"+(offset+54 < 100 ? "0" : "")+(offset+54 < 10 ? "0" : "")+(054+offset)+"110201=81#"
				+ "C6C0 1"+(offset+54 < 100 ? "0" : "")+(offset+54 < 10 ? "0" : "")+(054+offset)+"050201=80#"
				+ "C7I1 A"+(offset+47 < 100 ? "0" : "")+(offset+47 < 10 ? "0" : "")+(047+offset)+"250632=09#"
				+ "C8K0 A"+(offset+43 < 100 ? "0" : "")+(offset+43 < 10 ? "0" : "")+(043+offset)+"250202=06#"
				+ "C9C0 1"+(offset+34 < 100 ? "0" : "")+(offset+34 < 10 ? "0" : "")+(034+offset)+"380201=85#"
				+ "CAC0 1"+(offset+34 < 100 ? "0" : "")+(offset+34 < 10 ? "0" : "")+(034+offset)+"230201=82#"
				+ "CBC0 1"+(offset+34 < 100 ? "0" : "")+(offset+34 < 10 ? "0" : "")+(034+offset)+"110201=81#"
				+ "CCC0 1"+(offset+34 < 100 ? "0" : "")+(offset+34 < 10 ? "0" : "")+(034+offset)+"050201=80#"
				+ "CDI1 A0"+(027+offset)+"250632=09#"
				+ "CEK0 A"+(offset+23 < 100 ? "0" : "")+(offset+23 < 10 ? "0" : "")+(023+offset)+"250202=06#";
		//String pectabbarcode ="BTT0101]F0510250=#01C0 5255490201#02C0 5258150201#03C0 5255090201#04C0 5259180201#05C0 1000990201#06K0 A003250202#07C0 1000990201#08C0 5259490201#09I1 A007250632#0AC0 5474120201#0BC0 1000990201#10C0M1216020303#11C0M1216130303#12C0M1216310303#15C0MA224261006#16C0MA220260302#17C0 1000990201#18C0MA224440605#20C0M1196020303#21C0M1196130303#22C0M1196310303#25C0MA204261006#26C0MA200260302#27C0 1000990201#28C0MA204440605#30C0M1176020303#31C0M1176130303#32C0M1176310303#35C0MA184261006#36C0MA180260302#37C0 1000990201#38C0MA184440605#40C0 1000990201#41C0 1000990201#42C0 1000990201#45C0 1000990201#46C0 1000990201#47C0 1000990201#48C0 1000990201#77C0 1000990201#78C0M2159061408#80C0 1014050201#81C0 1014110201#82C0 1014230201#85C0 1014380201#86C0 1000990201#87C0 1000990201#88C0 1000990201#A1C0 5491490201=30#A2C0 5491430201=31#A3C0 5491300201=32#A4C0 5491130201=35#A5C0 5487490201=20#A6C0 5487430201=21#A7C0 5487300201=22#A8C0 5487130201=25#A9C0 5483490201=10#AAC0 5483430201=11#ABC0 5483300201=12#ACC0 5483130201=15#ADC0 E479250201=16#AEC0 5474490201BAG:#AFC0 5474390201=02#B0C0 5474350201/#B1C0 5474330201=03#B2C0 5474240201B/NO:#B3C0 5470490201=01#B4K0 E465250303=06#B5I1 E461260532=09#B6C0 5259420201/#B7C0 5259400201=82#B8C0 5255110201/#B9S0MA235250250#BAC0M1231020302TO#BBS0MA215250250#BCC0M1211020302VIA#BDS0MA195250250#BEC0M1191020302VIA#BFS0MA175250250#C0I1MC132474341=09#C1I1MA069254041=09#C2K0MA065250202=06#C3C0 1054380201=85#C4C0 1054230201=82#C5C0 1054110201=81#C6C0 1054050201=80#C7I1 A047250632=09#C8K0 A043250202=06#C9C0 1034380201=85#CAC0 1034230201=82#CBC0 1034110201=81#CCC0 1034050201=80#CDI1 A027250632=09#CEK0 A023250202=06#";
		
		askPrinter(pectabbarcode);
		System.out.println(pectabbarcode);
		System.out.println(getFeedback());
		if(sendToPrinter(stringToByteArray(barcode)))
		//if(false)
		{
			if(_lastMessageFromPrinter.contains("PROK"))
			{
				statutMessage+="successful.";
				successful = true;
			}
			else
			{
				statutMessage+="Failed - ";
				if(_lastMessageFromPrinter.contains("ERR"))
				{
					int i = _lastMessageFromPrinter.indexOf("ERR");
					switch(_lastMessageFromPrinter.charAt(i+3))
					{
						case '6': 
							statutMessage+="ERR6. PECTAB not available.";
							break;
						case '3':
							statutMessage+="ERR3 - PECTAB error at "+_lastMessageFromPrinter.substring(_lastMessageFromPrinter.indexOf("VSR#")+4);
							break;
						default:
							statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromPrinter+"";
							break;
					}
				}
				else
					statutMessage+=" Unknown reasons. Output from the printer :  "+_lastMessageFromPrinter+"";
				successful = false;
			}
		}
		else
		{
			successful = false;
			statutMessage+="Unable to reach printer.";
		}	
		_actionFeedback = statutMessage;
		_successfulAction = successful;
		return successful;
	}
	
	
	/**
	 * Convert the string to bytes and add the STX and ETX bytes.
	 * @param command String - command that needs formating to be send to the printer
	 * @return byte[] array containing the command surrounded by STX and ETX, ready to be sent to the printer
	 */
	private byte[] stringToByteArray(String command)
	{
		byte[] commandReadyForPrinter = null;
		ArrayList<Byte> AEACommand = new ArrayList<Byte>();
		AEACommand.add(STX);
		byte[] sToByte = command.getBytes();
		for(int i = 0; i < sToByte.length;i++)
		{
			AEACommand.add(sToByte[i]);
		}

		AEACommand.add(ETX);
		//For convenience a ArrayList was used to encode the message to be sent to the printer.
		//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.
		Byte[] buffer = (Byte[]) AEACommand.toArray(new Byte[0]);
		commandReadyForPrinter = toPrimitives(buffer);
		return commandReadyForPrinter;
	}
	
	/**
	 * Change the printer to the wanted operational mode (as specified in the parameters)
	 * 
	 * Uses AEA command XT and XC. More information in AEA TSpecs 2012 p35
	 * @param newPrinterMode - the wanted printer mode (checkin or ticketing)
	 * @return true if operational mode was successfully changed
	 */
	public boolean changeOperationalMode(PrinterMode newPrinterMode)
	{
		String statutMessage = "Change operational mode status to ";
		String command = "";
		boolean successful = false;
		if(newPrinterMode == PrinterMode.checkin)
		{
			command = "XC";
			statutMessage+="Check-in : ";
			if(sendToPrinter(stringToByteArray(command)))
			{
				if(_lastMessageFromPrinter.contains("XCOK"))
				{
					statutMessage+="Successful.\n";
					if(_lastMessageFromPrinter.length() > 4)
					{
						int i = _lastMessageFromPrinter.indexOf("XCOK")+4;
						String[] infos = _lastMessageFromPrinter.substring(i).split(_separator);
						if(!_lastMessageFromPrinter.substring(i).startsWith(_separator))
						{
						
							_PECTABLoaded.clear();
							int pectabCount = infos[0].length()/2;
							for(int j = 0; j < pectabCount ; j++)
							{
								_PECTABLoaded.add(infos[0].substring((j*2), (j*2)+2));
							}
							_PECTABinMemory = true;
							statutMessage+= "PECTABs in memory : ";
							for(String pectab : _PECTABLoaded)
							{
								statutMessage+=pectab+" ";
							}
							statutMessage+="\n";
						}
						else
						{
							statutMessage+="No PECTABs found in memory\n";
							_PECTABinMemory = false;
						}
						if(infos.length > 1)
							statutMessage+="Status info acc. PECTAB element steering command \"keep for confirmation and status\" : \n";
						for(int k = 1; k < infos.length;k++)
						{
							statutMessage+= "  Status info field number "+infos[k].substring(0,2)+" : "+infos[k].substring(2)+ "\n";
						}
					}
					_printerMode = PrinterMode.checkin;
					successful = true;
				}
				else
				{
					if(_lastMessageFromPrinter.contains("ERR"))
					{
						int i = _lastMessageFromPrinter.indexOf("ERR");
						switch(_lastMessageFromPrinter.charAt(i+3))
						{
						case '2': 
							statutMessage+="ERR2. Illogical command : "+_lastMessageFromPrinter.substring(i+4);
							break;
						case '7':
							statutMessage+="ERR7. A check-in/revalidation is in progress.";
							break;
							
						}
					}
					else
						statutMessage += "Unknown error. Output from printer : "+_lastMessageFromPrinter;
					successful = false;
				}
			}
			else
			{
				statutMessage+="Command could not be sent to the printer.";
				successful = false;
			}
		}
		else if(newPrinterMode == PrinterMode.ticketing)
		{
			command = "XT";
			statutMessage+="Ticketing : ";
			if(sendToPrinter(stringToByteArray(command)))
			{
				if(_lastMessageFromPrinter.contains("XTOK"))
				{
					statutMessage+="Successful.\n";
					if(_lastMessageFromPrinter.length() > 4)
					{
						int i = _lastMessageFromPrinter.indexOf("XTOK")+4;
						String[] infos = _lastMessageFromPrinter.substring(i).split(_separator);
						if(!_lastMessageFromPrinter.substring(i).startsWith(_separator))
						{
	
							_PECTABLoaded.clear();
							int pectabCount = infos[0].length()/2;
							for(int j = 0; j < pectabCount ; j++)
							{
								_PECTABLoaded.add(infos[0].substring((j*2), (j*2)+2));
							}
							_PECTABinMemory = true;
							statutMessage+= "PECTABs in memory : ";
							for(String pectab : _PECTABLoaded)
							{
								statutMessage+=pectab+" ";
							}
							statutMessage+="\n";
						}
						else
						{
							statutMessage+="No PECTABs found in memory\n";
							_PECTABinMemory = false;
						}
						if(infos.length > 1)
							statutMessage+="Status info acc. PECTAB element steering command \"keep for confirmation and status\" : \n";
						for(int k = 1; k < infos.length;k++)
						{
							statutMessage+= "  Status info field number "+infos[k].substring(0,2)+" : "+infos[k].substring(2)+ "\n";
						}
					}
					_printerMode = PrinterMode.ticketing;
					successful = true;
				}
				else
				{
					if(_lastMessageFromPrinter.contains("ERR"))
					{
						int i = _lastMessageFromPrinter.indexOf("ERR");
						switch(_lastMessageFromPrinter.charAt(i+3))
						{
						case '2': 
							statutMessage+="ERR2. Illogical command : "+_lastMessageFromPrinter.substring(i+4);
							break;
						case '7':
							statutMessage+="ERR7. A check-in/revalidation is in progress.";
							break;
							
						}
					}
					else
						statutMessage += "Unknown error. Output from printer : "+_lastMessageFromPrinter;
					successful = false;
				}
			}
			else
			{
				statutMessage+="Command could not be sent to the printer.";
				successful = false;
			}
		}
		else
		{
			statutMessage+="No new printer mode selected. No action performed.";
			successful = false;
		}
		_actionFeedback = statutMessage;
		_successfulAction = successful;
		return successful;
	}
	
	/**
	 * This function is used to send any AEA command. If a specific function has already been written for it (example printing), it is better to use this function as it contains error handling.
	 * @param request String - The AEA you want to send to the printer
	 * @return true if command was successfully sent to the printer
	 */
	public boolean askPrinter(String request){
		boolean succesful = false;
		String statutMessage = "Ask status : ";
		_lastCommandSent = request;
		if(sendToPrinter(stringToByteArray(request)))
		{
			statutMessage+="Sent.";
			succesful = true;
		}
		else
		{
			statutMessage+="Failed.";
			succesful = false;
		}
		
		_actionFeedback = statutMessage;
		_successfulAction = succesful;
		return succesful;
	}
	/**
	 * Display all the printer status information
	 * 	-PECTABs loaded in the printer memory
	 * 	-logos loaded in the printer memory
	 * 	-fonts loaded in the printer memory
	 * 	-templates loaded in the printer memory
	 * 	-program version of the programm in the printer memory
	 * 	-hardcoded transaction code of the printer
	 * 	-bin setup 
	 * in an understandable by humans fashion
	 * Warning: Does not automatically update the status information, only display them from the last time it was requested
	 * To request an update for status information, call requestPrinterGeneralStatus() and updateUpdatePectabAndPrinterModeInformations()
	 * Those two functions are being called in the constructor
	 * 
	 * Could be changed to return a String with all information if needed
	 * @throws Exception throws Exception if printer wasn't properly initialized
	 */
	public void displayPrinterStatus() throws Exception
	{
		if(_printerInit)
		{
			requestPrinterGeneralStatus();
			System.out.print("Printer emulation : ");
			if(_printerEmulation == PrinterEmulation.ATB)
				System.out.println("ATB.");
			else if(_printerEmulation == PrinterEmulation.BTP)
				System.out.println("BTP.");
			else
				System.out.println("Service.");
			
			System.out.print("Printer operational mode : ");
			if(_printerMode == PrinterMode.ticketing)
				System.out.println("Ticketing.");
			else if(_printerMode == PrinterMode.checkin)
				System.out.println("Check-in.");
			else
				System.out.println("This printer doesn't have a mode change capability.");
			
			if(_PECTABinMemory)
			{
				System.out.print("PECTAB in the printer memory : ");
				for(int i =0; i < _PECTABLoaded.size();i++)
				{
					System.out.print(_PECTABLoaded.get(i)+" ");
				}
				System.out.println();
			}
			else
			{
				System.out.println("No parametric table (PECTAB) found in the printer memory.");
			}
			if(_logosInMemory)
			{
				System.out.print("Logos in the printer memory : ");
				for(int i =0; i < _logosLoaded.size();i++)
				{
					System.out.print(_logosLoaded.get(i)+" ");
				}
				System.out.println();
			}
			else
			{
				System.out.println("No logos found in the printer memory.");
			}
			
			System.out.println("Program version: "+_programVersion);
			
			if(_templatesInMemory)
			{
				System.out.print("Templates in the printer memory : ");
				for(int i =0; i < _templatesLoaded.size();i++)
				{
					System.out.print(_templatesLoaded.get(i)+" ");
				}
				System.out.println();
			}
			else
			{
				System.out.println("No templates found in the printer memory.");
			}
			
			if(_fontsInMemory)
			{
				System.out.print("Fonts in the printer memory : ");
				for(int i =0; i < _fontsLoaded.size();i++)
				{
					System.out.print(_fontsLoaded.get(i)+" ");
				}
				System.out.println();
			}
			else
			{
				System.out.println("No fonts found in the printer memory.");
			}
			
			System.out.println("Printer transaction code : "+_transactionCode);
			
			System.out.println("Bin setup : ");
			for(int i = 0; i < 3; i++)
			{
				char[] currentBinParameters = _binParameters.get(i).toCharArray();
				System.out.print("  Bin "+currentBinParameters[0]+" : ");
				System.out.print("coupon type = "+currentBinParameters[1]+", ");
				if(currentBinParameters[2] == 'F')
					System.out.print("verify on, ");
				else
					System.out.print("verify off, ");
				switch(currentBinParameters[3])
				{
				case 'A':
					System.out.println("coupon reading in ascending order.");
					break;

				case 'D':
					System.out.println("coupon reading in descending order.");
					break;

				case 'N':
					System.out.println("preencoded stock control is off.");
					break;
					
				}
			}
			System.out.println("  Default bin : "+_defaultBin.charAt(1)+".");
			System.out.println("  Front feed parameters : verify "+(_frontFeedParameter.charAt(1) == 'V' ? "on" : "off")+".");
			System.out.println("  Status request parameters : info "+(_binStatusRequestParameter.charAt(1) == 'S' ? "included" : "not included")+".");
			
		}
		else
		{
			throw new Exception("The printer was not correctly initialized.");
		}
	}
}
