<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.detrics.iatci.*, com.detrics.iatci.edi.elements.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>MarenHersch IATCI-broker</title>
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/iosiatci.css" />
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
</head>
<body>

<% Session iatcisession = null; 
	String ex="", sID="";
   if (request.getParameter("sessionID")!=null) {
		sID = request.getParameter("sessionID");
		iatcisession = new Session();
		iatcisession.findByPK(request.getParameter("sessionID"));
	}
%>

<div id='topbar'>
	<div id='titleleft'>
		MarenHersch<br>
		IATCI-broker
	</div>
	<div id='titleright'>
		List of messages
	</div>
</div>

<div id='adminmenu'>
	<ul>
		<li><a href=''>Risk Factors</a></li>
		<li><a href=''>Menu 1</a></li>
		<li class='active'><a href=''>Menu 1</a></li>
		<li><a href=''>Menu 1</a></li>
	</ul>
</div>

<div id='right'>

	<div id='content'>
		
		<div id='tabbar'>
			<div class='tab active'>
				IATCI-messages
			</div>
			<div class='tab'>
				IATCI-messages
			</div>
			<div class='tabsearch'>
				<input type='text' name='term' class='term'>
			</div>
		</div>
		
		<div class='datalist'>
			<table class='grid'>
				<tr><th><input type='checkbox'></th><th>test</th><th>nog een test</th><th>volle breedte</th></tr>
				<% for (Session s : Session.getSessions(0)) { ex="";  if (s.getSessionID().equals(sID)) { ex="active"; } %>
					<tr class='cp <%= ex %>' onclick="location.href='<%= request.getContextPath() %>/sessions/sessions.jsp?sessionID=<%= s.getSessionID() %>'">
						<td style='width:20px'><input type='checkbox'></td>
						<td><%= s.getSessionID() %></td>
						<td><%= s.getISender().getAirliner3() %></td>
						<td><%= s.getiRecipient().getAirliner3() %></td>
						<td><%= s.getUTCtime() %></td>
						<td><%= s.getUTCdate() %></td>
						<td><% if (s.isPendingIn()) { %>I<% } %>
							<% if (s.isPendingOut()) { %>O<% } %>
						</td>
						
						<td><%= s.getnWarnings() %></td>
					</tr>
				<% } %>
			</table>
		</div>		

	</div>
	
	<% if (iatcisession!=null) { %>
	<div id='content'>
		<div class='datatxt'>
			<code>
			<% for (Message msg : iatcisession.getMessages()) { String ms = ""; %>
				<% if (msg.getMessage()!=null) { ms = msg.getMessage().replace("'", "'<br>"); } %>
				<div style='float:right'><i><%= msg.getCreatedDate() %> <%= msg.getCreatedTime() %></i></div>
				<B><%= msg.getMessageType() %> <%= msg.getIOw() %></B><br>
				<%= ms %><br>
				<% for (WAD w : msg.getWarnings()) { %>
					<%= w.get9845() %>:<%= w.get4440() %>
				<% } %>
				<br>
			<% } %>
			</code>
		</div>
	</div>
	<% } %>

</div>

</body>
</html>